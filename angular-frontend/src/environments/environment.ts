// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBbcxXDdLhHeKex0AXPalNbsagyZloRz7o",
    authDomain: "protea-work-active-staging.firebaseapp.com",
    projectId: "protea-work-active-staging",
    storageBucket: "protea-work-active-staging.appspot.com",
    messagingSenderId: "655261598034",
    appId: "1:655261598034:web:49449edfa8b3e62c1b7a8b"
  },
  basedataurl: "https://staging-dot-protea-work-active.appspot.com/",

};
