export interface User {
    UserId : number;
    UserName : string;
    FirstName : string;
    LastName : string;
    EmailAddress : string;
    FirebaseUid : string;
    AccessLevel : number;
    DivisionId: number;
    DailyGoals? : string;
    ActiveDays? : string;
    WeightKg? : number;
    HeightCm? : number;
    CompanyId? : number;
    CompanyName? : string;
    DivisionName? : string;
    CountryId? : number;
    Country? : string;
}

/** Used to get user info to be shown in the table in the superusers component.
 *  Currently only used for superusers component.  */
export interface UserInfo {
    UserId: number;
    UserName: string;
    FirstName: string;
    LastName: string;
    EmailAddress: string;
    AccessLevel: number;
    DivisionId: number;
    CompanyId?: number;
    CompanyName?: string;
    DivisionName?: string;
    CountryId?: number;
    Country?: string;
    DateRegistered: Date;
}

export interface Company {
    CompanyId : number;
    CompanyName : string;
    CountryId : number;
    Countrty? : string;
}

export interface Division {
    DivisionId : number;
    DivisionCode : string;
    DivisionName : string;
    CompanyId : number;
    CompanyName? : string;
    Country? : string;
    CountryId? : number;
}

export interface ChallengeTeam {
    TeamId: number;
    ChallengeId: number;
    TeamName: string;
    Users?: User[];
    Score?: number;
}

export interface Participant {
    UserId : number;
    TeamId : number;
    ChallengeId : number;
    Points : number;
}

export interface Challenge {
    ChallengeId : number;
    ChallengeName : string;
    StartDate : string;
    EndDate : string;
    Description: string;
    ChallengeTypeId : number;
    DailyGoal : number;
    ChallengeTypeName? : string;
    Avliability? : ChallengeAvaliability[];
    isTeam?: boolean;
    CountryId?: number;
    CompanyId?: number;
}

export interface ChallengeType{
    ChallengeTypeId : number;
    ChallengeTypeName : string;
    ParticipantTypeId : number;
    PointsPerObjective : number;
}

export interface SessionType{
    SessionTypeId : number;
    SessionTypeName : string;
    TimeSeconds : number;
    Theme: string;
}

export interface Session{
    SessionId : number;
    UserId : number;
    DateAndTime : Date;
    SessionTypeId : number;
}

export interface Country {
     CountryName : string;
     CountryId : number;
}

export interface ChallengeAvaliability {
    ChallengeId : number;
    DivisionId? : number;
    CountryId? : number;
}

export interface ChallengeReport {
    Buckets: Date[];
    StartingUserCounts: ChallengeReportSlice[];
    StartingCountUsersInChallenges: ChallengeReportSliceWithChallengeInfo[];
    TotalUsers: ChallengeReportSlice[];
    ActiveUsers: ChallengeReportSlice[];
    UsersInChallenges: ChallengeReportSliceWithChallengeInfo[];
    ActiveUsersInChallenges: ChallengeReportSlice[];
}

export interface ChallengeReportSlice {
    Bucket: Date;
    UserCount: number;
    CountryId: number;
    CompanyId: number;
    DivisionId: number;
}

export interface ChallengeReportSliceWithChallengeInfo extends ChallengeReportSlice {
    ChallengeId: number;
    ChallengeEndDate: Date;
}
