﻿import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { flatMap, switchMap } from 'rxjs/operators';
import { DataValue } from '../app/helpers/datavalue';
import { environment } from '../environments/environment';
import { Challenge, ChallengeAvaliability, ChallengeReport, ChallengeTeam, ChallengeType, Company, Country, Division, Participant, Session, SessionType, User, UserInfo } from './structures';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};



@Injectable()
export class AppService {
  readonly base : string = environment.basedataurl;
  readonly usersUrl: string = "api/users";
  readonly usersAdvUrl: string = "api/users/adv";
  readonly userUpdateUrl: string = "api/updateUser";
  readonly userReportUrl: string = "api/userreports";
  readonly usersAccessUrl: string = "api/users/access";
  readonly divisionUrl: string = "api/divisions";
  readonly companiesUrl: string = "api/companies";
  readonly challengeUrl: string = "api/challenges";
  readonly activeChallengeUrl: string = "api/activechallenges";
  readonly challengeTypeUrl: string = "api/challengeTypes";
  readonly sessionUrl: string = "api/sessions";
  readonly sessionTypeUrl: string = "api/sessionTypes";
  readonly countryUrl: string = "api/countries";
  readonly challengeAvaliableUrl: string = "api/challengeAvailableTo"
  readonly teamUrl: string = "api/challengeTeams/challengeId=";
  readonly challengeParticipantsUrl: string = "api/participants/users/challengeId=";
  readonly challengeAllParticipantsUrl: string = "api/participants/allteams/challengeId=";
  readonly challengeAddMultipleDivisionAvliabilityUrl: string = "api/challenges/multidivisions";
  readonly sessionDataUrl = "api/SessionsOverviewReport";
  readonly sessionChallengeDataUrl = "api/challengeActiveOverviewReport";
  readonly languagesUrl = "api/languages";
  readonly checkDivisionUrl = "api/divisionExistanceCheck";
  readonly challengeWinnerUrl = "api/challengewinner";

  companyToForce: Company;

  constructor(private http: HttpClient) {

  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  };


  getUsers(): Observable<UserInfo[]> {
    return this.http.get<UserInfo[]>(this.base + this.usersUrl, httpOptions);
  }

  getUserByUid(uid): Observable<any> {
    let usersData = this.http.get<User>(this.base + this.usersAdvUrl + "/FirebaseUid=" + uid, httpOptions);
    return usersData;
  }

  createUser(user: JSON): Observable<JSON> {
    return this.http.post<JSON>(this.base + this.usersUrl, user, httpOptions);
  }

  updateUser(user: JSON, updateAccess: boolean): Observable<JSON> {
    let base = this.http.put<JSON>(this.base + this.userUpdateUrl, user, httpOptions);
    if (!updateAccess) return base;
    else return base.pipe(switchMap((result) => {
      return this.http.put<JSON>(this.base + this.usersAccessUrl, user, httpOptions);
    }));
  }

  deleteUser(user: number): Observable<string> {
    return this.http.delete<string>(this.base + this.usersUrl + "/userId=" + user);
  }

  getDivisions(): Observable<Division[]> {
    let divisionData = this.http.get<Division[]>(this.base + this.divisionUrl);
    return divisionData;
  }

  getDivisionById(id): Observable<Division> {
    let divisionData = this.http.get<Division>(this.base + this.divisionUrl + "/DivisionId=" + id);
    return divisionData;
  }


  createDivision(division: JSON): Observable<JSON> {
    return this.http.post<JSON>(this.base + this.divisionUrl, division, httpOptions);
  }

  checkDivision(divisionId, divisionCode): Observable<JSON> {
    let data={ DivisionId: divisionId, DivisionCode: divisionCode };
    console.log("checkDivision Data packet",data);
    return this.http.post<JSON>(this.base + this.checkDivisionUrl,
      data
      , httpOptions);
  }


  updateDivision(division: JSON): Observable<JSON> {
    return this.http.put<JSON>(this.base + this.divisionUrl, division, httpOptions);
  }

  deleteDivision(division: number): Observable<JSON> {
    return this.http.delete<JSON>(this.base + this.divisionUrl + "/divisionId=" + division);
  }


  forceCompany(com: Company) {
    this.companyToForce = com;
  }

  getCompanies(): Observable<Company[]> {
    if (this.companyToForce) return of([this.companyToForce])
    let comapniesData = this.http.get<Company[]>(this.base + this.companiesUrl);
    return comapniesData;
  }

  getLanguages(): Observable<any[]> {
    let data = this.http.get<any[]>(this.base + this.languagesUrl);
    return data;
  }

  getCompanyById(id): Observable<Company> {
    let data = this.http.get<Company>(this.base + this.companiesUrl + "/CompanyId=" + id);
    return data;
  }

  createCompany(company: JSON): Observable<JSON> {
    return this.http.post<JSON>(this.base + this.companiesUrl, company, httpOptions);
  }

  updateCompany(company: JSON): Observable<JSON> {
    return this.http.put<JSON>(this.base + this.companiesUrl, company, httpOptions);
  }

  deleteCompany(company: number): Observable<JSON> {
    return this.http.delete<JSON>(this.base + this.companiesUrl + "/companyId=" + company);
  }


  getChallenges(): Observable<Challenge[]> {
    let challengeData = this.http.get<Challenge[]>(this.base + this.challengeUrl);
    return challengeData;
  }

  getActiveChallenges(): Observable<Challenge[]> {
    let challengeData = this.http.get<Challenge[]>(this.base + this.activeChallengeUrl);
    return challengeData;
  }

  createChallenge(challenge: JSON): Observable<JSON> {
    return this.http.post<JSON>(this.base + this.challengeUrl, challenge, httpOptions);
  }

  updateChallenge(challenge: JSON): Observable<JSON> {
    console.log(challenge);
    return this.http.put<JSON>(this.base + this.challengeUrl, challenge, httpOptions);
  }

  getChallengeAvaliability(challengeId): Observable<ChallengeAvaliability[]> {
    let challengeData = this.http.get<ChallengeAvaliability[]>(this.base + this.challengeAvaliableUrl + "/challengeId=" + challengeId);
    return challengeData;
  }

  removeCountryFromChallenge(challengeId,countryId): Observable<JSON> {
    return this.http.delete<JSON>(this.base + this.challengeUrl+"/countries/challengeId="+challengeId+"&countryId="+countryId);
  }

  removeDivisionFromChallenge(challengeId,divId): Observable<JSON> {
    return this.http.delete<JSON>(this.base + this.challengeUrl+"/divisions/challengeId="+challengeId+"&divisionId="+divId);
  }

  addCountryFromChallenge(challengeId,countryId): Observable<JSON> {
    let data={};
    return this.http.post<JSON>(this.base + this.challengeUrl+"/countries/challengeId="+challengeId+"&countryId="+countryId,data);
  }

  addDivisionFromChallenge(challengeId,divId): Observable<JSON> {
    let data={};
    return this.http.post<JSON>(this.base + this.challengeUrl+"/divisions/challengeId="+challengeId+"&divisionId="+divId,data);
  }

  createMultipleDivisionAvaliabilities(divisions: JSON): Observable<JSON> {
    let resp = this.http.post<JSON>(this.base + this.challengeAddMultipleDivisionAvliabilityUrl, divisions, httpOptions);
    return resp;
  }

  deleteMultipleDivisionAvaliabilities(divisions: JSON): Observable<JSON> {
    let resp = this.http.post<JSON>(this.base + this.challengeAddMultipleDivisionAvliabilityUrl + "delete", divisions, httpOptions);
    return resp;
  }


  deleteChallenge(challenge: number): Observable<JSON> {
    return this.http.delete<JSON>(this.base + this.challengeUrl + "/challengeId=" + challenge);
  }

  getChallengeTypes(): Observable<ChallengeType[]> {
    let challengesData = this.http.get<ChallengeType[]>(this.base + this.challengeTypeUrl);
    return challengesData;
  }

  createChallengeType(ct: JSON): Observable<JSON> {
    return this.http.post<JSON>(this.base + this.challengeTypeUrl, ct, httpOptions);
  }

  updateChallengeType(ct: JSON): Observable<JSON> {

    return this.http.put<JSON>(this.base + this.challengeTypeUrl, ct, httpOptions);
  }

  deleteChallengeType(ct: number): Observable<JSON> {
    console.log(this.base + this.challengeTypeUrl + "/challengeTypeId=" + ct);
    return this.http.delete<JSON>(this.base + this.challengeTypeUrl + "/challengeTypeId=" + ct);
  }



  getSessions(): Observable<Session[]> {
    let sessionData = this.http.get<Session[]>(this.base + this.sessionUrl);
    return sessionData;
  }

  createSession(session: JSON): Observable<JSON> {
    return this.http.post<JSON>(this.base + this.sessionUrl, session, httpOptions);
  }

  deleteSession(session: number): Observable<JSON> {
    return this.http.delete<JSON>(this.base + this.sessionUrl + "/sessionId=" + session);
  }

  getSessionTypes(): Observable<SessionType[]> {
    let sessionsData = this.http.get<SessionType[]>(this.base + this.sessionTypeUrl);
    return sessionsData;
  }

  createSessionType(st: JSON): Observable<JSON> {
    return this.http.post<JSON>(this.base + this.sessionTypeUrl, st, httpOptions);
  }

  updateSessionType(st: JSON): Observable<JSON> {
    return this.http.put<JSON>(this.base + this.sessionTypeUrl, st, httpOptions);
  }

  deleteSessionType(st: number): Observable<JSON> {
    return this.http.delete<JSON>(this.base + this.sessionTypeUrl + "/sessionTypeId=" + st);
  }

  getChallengeTeams(challengeId): Observable<ChallengeTeam[]> {
    let team = this.http.get<ChallengeTeam[]>(this.base + this.teamUrl + challengeId);
    return team;
  }

  createTeam(team: JSON): Observable<JSON> {
    return this.http.post<JSON>(this.base + "api/challengeTeams", team, httpOptions);
  }

  updateTeam(team: JSON): Observable<JSON> {
    return this.http.put<JSON>(this.base + "api/challengeTeams", team, httpOptions);
  }

  getUserParticipantsByChallengeId(challengeId): Observable<Participant[]> {
    let team = this.http.get<Participant[]>(this.base + this.challengeParticipantsUrl + challengeId);
    return team;
  }

  getAllTeamParticipantsByChallengeId(challengeId): Observable<Participant[]> {
    let team = this.http.get<Participant[]>(this.base + this.challengeAllParticipantsUrl + challengeId);
    return team;
  }

  getCountries(): Observable<Country[]> {
    let countryData = this.http.get<Country[]>(this.base + this.countryUrl);
    return countryData;
  }

  getSessionData(type, start, end): Observable<JSON> {
    let d = {
      StartDate: start,
      EndDate: end,
      AggregateRange: type
    }
    console.log(d);
    return this.http.post<JSON>(this.base + this.sessionDataUrl, d, httpOptions);
  }


    getSessionChallengeData(type: number, start: string, end: string): Observable<ChallengeReport> {
        let d = {
            StartDate: start,
            EndDate: end,
            AggregateRange: type
        }
        try {
            return this.http.post<any>(this.base + this.sessionChallengeDataUrl, d, httpOptions).pipe(
                flatMap(rawResponse => {
                    const d = new DataValue<ChallengeReport>(rawResponse);
                    return of({
                        Buckets: d.get("Buckets").map(n => new Date(n.asString())),
                        StartingUserCounts: d.get("StartingUserCounts").map(slice => ({
                            Bucket: new Date(slice.get("Bucket").asString()),
                            UserCount: slice.get("UserCount").asNumber(),
                            CountryId: slice.get("CountryId").asNumber(),
                            CompanyId: slice.get("CompanyId").asNumber(),
                            DivisionId: slice.get("DivisionId").asNumber()
                        })),
                        StartingCountUsersInChallenges: d.get("StartingCountUsersInChallenges").map(slice => ({
                            Bucket: new Date(slice.get("Bucket").asString()),
                            UserCount: slice.get("UserCount").asNumber(),
                            CountryId: slice.get("CountryId").asNumber(),
                            CompanyId: slice.get("CompanyId").asNumber(),
                            DivisionId: slice.get("DivisionId").asNumber(),
                            ChallengeId: slice.get("ChallengeId").asNumber(),
                            ChallengeEndDate: new Date(slice.get("ChallengeEndDate").asString())
                        })),
                        TotalUsers: d.get("TotalUsers").map(slice => ({
                            Bucket: new Date(slice.get("Bucket").asString()),
                            UserCount: slice.get("UserCount").asNumber(),
                            CountryId: slice.get("CountryId").asNumber(),
                            CompanyId: slice.get("CompanyId").asNumber(),
                            DivisionId: slice.get("DivisionId").asNumber()
                        })),
                        ActiveUsers: d.get("ActiveUsers").map(slice => ({
                            Bucket: new Date(slice.get("Bucket").asString()),
                            UserCount: slice.get("UserCount").asNumber(),
                            CountryId: slice.get("CountryId").asNumber(),
                            CompanyId: slice.get("CompanyId").asNumber(),
                            DivisionId: slice.get("DivisionId").asNumber()
                        })),
                        UsersInChallenges: d.get("UsersInChallenges").map(slice => ({
                            Bucket: new Date(slice.get("Bucket").asString()),
                            UserCount: slice.get("UserCount").asNumber(),
                            CountryId: slice.get("CountryId").asNumber(),
                            CompanyId: slice.get("CompanyId").asNumber(),
                            DivisionId: slice.get("DivisionId").asNumber(),
                            ChallengeId: slice.get("ChallengeId").asNumber(),
                            ChallengeEndDate: new Date(slice.get("ChallengeEndDate").asString())
                        })),
                        ActiveUsersInChallenges: d.get("ActiveUsersInChallenges").map(slice => ({
                            Bucket: new Date(slice.get("Bucket").asString()),
                            UserCount: slice.get("UserCount").asNumber(),
                            CountryId: slice.get("CountryId").asNumber(),
                            CompanyId: slice.get("CompanyId").asNumber(),
                            DivisionId: slice.get("DivisionId").asNumber()
                        }))
                    })
                })
            );
        } catch (e) {
            console.error(e);
        }
    }

  getUserCounts(): Observable<any[]> {
    let data = this.http.get<any[]>(this.base + this.userReportUrl);
    return data;
  }

  getChallengeWinners(cid,isTeams): Observable<any[]> {
    let data = this.http.get<any[]>(this.base + this.challengeWinnerUrl+(isTeams?"/teams=":"/participant=")+cid);
    return data;
  }


}
