/** Helper class to deserialize a json object in a type-safe way
 * */

export class DataValue<T> {
    private readonly value: any
    private readonly path: string | undefined

    constructor(value: any, path?: string) {
        this.value = value
        this.path = path
    }

    public asString(): string {
        if (typeof this.value === "string") {
            return this.value
        } {
            throw this.failure("string")
        }
    }

    public asNumber(): number {
        if (typeof this.value === "number") {
            return this.value
        } {
            throw this.failure("number")
        }
    }

    public to<R>(func: (v: this) => R): R {
        return func(this)
    }

    public map<R>(func: (v: DataValue<T extends (infer A)[] ? A : never>) => R): R[] {
        if (Array.isArray(this.value)) {
            return this.value.map((v, i) => func(new DataValue(v, this.subPath(i))))
        } else {
            throw this.failure("array")
        }
    }

    public get<K extends keyof T>(key: K): DataValue<T[K]> {
        if (typeof this.value === "object" && this.value !== null && key in this.value) {
            return new DataValue(this.value[key], this.subPath(key))
        } else if (typeof key === "number") {
            throw this.failure(`array with index ${key}`)
        } else {
            throw this.failure(`object with key ${key}`)
        }
    }

    public getString(key: keyof T): string {
        return this.get(key).asString()
    }

    public getNumber(key: keyof T): number {
        return this.get(key).asNumber()
    }

    private failure(expectedType: string) {
        `Expected ${this.path} to be a ${expectedType}: ${this.value} (type: ${typeof this.value})`
    }

    private subPath(key: string | number | symbol): string {
        if (this.path === undefined) {
            return String(key)
        } else {
            return `${this.path}.${String(key)}`
        }
    }
}

/** Example usage */

interface Person {
    name: Name
    birthday: Date
    nonBirthdays: Date[]
    addresses: Address[]
}

interface Name {
    name: string
    surname: string
}

interface Address {
    street: string
    no: number
}

function parse(resp: any): Person {
    const d = new DataValue<Person>(resp)
    return {
        name: d.get("name").to(n => ({
            name: n.getString("name"),
            surname: n.get("surname").asString()
        })),
        birthday: new Date(d.get("birthday").asString()),
        nonBirthdays: d.get("nonBirthdays").map(n => new Date(n.asString())),
        addresses: d.get("addresses").map(a => ({
            street: a.get("street").asString(),
            no: a.get("no").asNumber()
        }))
    }
}
