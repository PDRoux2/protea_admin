import {Component } from '@angular/core';
import { navItemsAdm,navItemsCom } from '../../_nav';

import { AuthenticationService } from '../../authentication.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './admin-layout.component.html'
})
export class DefaultLayoutComponent {
  public sidebarMinimized = false;
  public navItems = navItemsCom;

  constructor(public authService:AuthenticationService) {
        this.authService.userChannel.subscribe(u=>{
             let lev=this.authService.getUserDetail().AccessLevel;
             this.navItems=(lev>1)?navItemsAdm:navItemsCom;
        });
  }

  logout()
  {
    this.authService.logout();
  }

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

}
