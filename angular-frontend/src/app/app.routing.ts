import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { RouteGuardService } from './route-guard.service';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';

export const routes: Routes = [
  {
    path: '',
    canActivate: [RouteGuardService],
    redirectTo: 'superdashboard',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    canActivate: [RouteGuardService],
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'superdemographics',
        loadChildren: () => import('./views/superdemographics/superdemographics.module').then(m => m.SuperDemographicsModule)
      },
      {
        path: 'superchallengereport',
        loadChildren: () => import('./views/superchallengereport/superchallengereport.module').then(m => m.SuperChallengeReportModule)
      },
      {
        path: 'companies',
        loadChildren: () => import('./views/supercompanies/companies.module').then(m => m.CompaniesModule)
      },
      {
        path: 'superdashboard',
        loadChildren: () => import('./views/superdashboard/superdashboard.module').then(m => m.SuperDashboardModule)
      },
      {
        path: 'challenges',
        loadChildren: () => import('./views/superchallenges/superchallenges.module').then(m => m.SuperChallengesModule)
      },
      {
        path: 'divisions',
        loadChildren: () => import('./views/superdivisions/superdivisions.module').then(m => m.SuperDivisionsModule)
      },
      {
        path: 'superusers',
        loadChildren: () => import('./views/superusers/superusers.module').then(m => m.SuperUsersModule)
      },
      {
        path: 'supersessions',
        loadChildren: () => import('./views/supersessions/supersessions.module').then(m => m.SuperSessionsModule)
      },
      {
        path: 'supersessiontypes',
        loadChildren: () => import('./views/supersessiontype/supersessiontypes.module').then(m => m.SuperSessionTypesModule)
      },
      {
        path: 'superchallengetypes',
        loadChildren: () => import('./views/superchallengetype/superchallengetype.module').then(m => m.SuperChallengeTypeModule)
      },
      {
        path: 'challengewinnersreport',
        loadChildren: () => import('./views/challengewinners/challengewinners.module').then(m => m.ChallengeWinnersModule)
      },

    ]
  },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
