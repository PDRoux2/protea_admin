import { INavData } from '@coreui/angular';

export const navItemsAdm: INavData[] = [
  {
    title: true,
    name: $localize`:@@menu_reports:Reports`,
  },
  {
    name: $localize`:@@menu_usage:Usage`,
    url: '/superdashboard',
    icon: 'icon-speedometer',
  },
  {
    name: $localize`:@@menu_demographics:Demographics`,
    url: '/superdemographics',
    icon: 'icon-speedometer',
  },
  {
    name: $localize`:@@menu_challenge:Challenge Report`,
    url: '/superchallengereport',
    icon: 'icon-speedometer',
  },
  {
    name: $localize`:@@menu_challengeresult:Challenge Results`,
    url: '/challengewinnersreport',
    icon: 'icon-speedometer',
  },
  {
    title: true,
    name: $localize`:@@menu_admin:Admin`
  },
  {
    name: $localize`:@@menu_users:Users`,
    url: '/superusers',
    icon: 'cui-people'
  },
  {
    name: $localize`:@@menu_companies:Companies`,
    url: '/companies',
    icon: 'cui-home'
  },
  {
    name: $localize`:@@menu_divisions:Divisions`,
    url: '/divisions',
    icon: 'icon-pencil'
  },
  /*{
    name: $localize`:@@menu_challengetypes:Challenge Types`,
    url: '/superchallengetypes',
    icon: 'icon-badge'
  },*/
  {
    name: $localize`:@@menu_challenges:Challenges`,
    url: '/challenges',
    icon: 'cui-puzzle'
  },
  {
    name: $localize`:@@menu_sessiontypes:Session Types`,
    url: '/supersessiontypes',
    icon: 'icon-paper-plane'
  },

];

export const navItemsCom: INavData[] = [
  {
    title: true,
    name: $localize`:@@menu_reports:Reports`,
  },
  {
    name: $localize`:@@menu_usage:Usage`,
    url: '/superdashboard',
    icon: 'icon-speedometer',
  },
  {
    name: $localize`:@@menu_demographics:Demographics`,
    url: '/superdemographics',
    icon: 'icon-speedometer',
  },
  {
    name: $localize`:@@menu_challenge:Challenge Report`,
    url: '/superchallengereport',
    icon: 'icon-speedometer',
  },
  {
    name: $localize`:@@menu_challengeresult:Challenge Results`,
    url: '/challengewinnersreport',
    icon: 'icon-speedometer',
  },
  {
    title: true,
    name: $localize`:@@menu_admin:Admin`
  },
  {
    name: $localize`:@@menu_users:Users`,
    url: '/superusers',
    icon: 'cui-people'
  },
  {
    name: $localize`:@@menu_divisions:Divisions`,
    url: '/divisions',
    icon: 'icon-pencil'
  },
  {
    name: $localize`:@@menu_challenges:Challenges`,
    url: '/challenges',
    icon: 'cui-puzzle'
  },
];
