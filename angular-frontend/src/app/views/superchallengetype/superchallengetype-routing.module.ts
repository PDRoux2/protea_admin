import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuperChallengeTypeComponent } from './superchallengetype.component';

const routes: Routes = [
  {
    path: '',
    component: SuperChallengeTypeComponent,
    data: {
      title: 'Dashboard'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuperChallengeTypeRoutingModule {}
