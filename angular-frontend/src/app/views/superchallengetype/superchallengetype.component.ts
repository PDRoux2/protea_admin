import { Component, OnInit, OnDestroy, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { HttpClient } from '@angular/common/http';
import { AppService } from '../../app.service';
import { Subject, Observable } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { ChallengeType, Company } from '../../structures';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  templateUrl: 'superchallengetype.component.html'
})
export class SuperChallengeTypeComponent implements OnDestroy, OnInit {

  challengeTypeData: ChallengeType[];

  @ViewChild(DataTableDirective) dtElement: DataTableDirective;
  @ViewChild('addChallengeTypeModal') public addChallengeTypeModal: ModalDirective;
  @ViewChild('editChallengeTypeModal') public editChallengeTypeModal: ModalDirective;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;

  selectedChallengeType: ChallengeType;

  dtOptions: DataTables.Settings = {
    "columnDefs": [{
      "targets": 0,
      "orderable": false
    }]
  };

  dtTrigger: Subject<any> = new Subject();
  isLoading=true;
  firstLoad=true;


  constructor(private http: HttpClient, private service: AppService) {

  }

  editChallengeType(challengeType: ChallengeType) {
    this.selectedChallengeType = challengeType;

    (<any>document.getElementById("edit_name")).value = challengeType.ChallengeTypeName;
    (<any>document.getElementById("edit_part")).value = challengeType.ParticipantTypeId;
    (<any>document.getElementById("edit_points")).value = challengeType.PointsPerObjective;

    this.editChallengeTypeModal.show();
  }

  revealCompany(s: string) {
    document.getElementById("details").hidden = false;
    document.getElementById("details_name").innerText = s;
  }

  updateChallengeType() {
    this.editChallengeTypeModal.hide();

    let challengeType: ChallengeType = this.selectedChallengeType;

    challengeType.ChallengeTypeName = (<any>document.getElementById("edit_name")).value;
    challengeType.ParticipantTypeId = (<any>document.getElementById("edit_part")).value;
    challengeType.PointsPerObjective = (<any>document.getElementById("edit_points")).value;

    this.service.updateChallengeType(<JSON><any>challengeType).subscribe((data)=>{
        this.loadData();
      }
    );
  }


  createChallengeType() {
    this.addChallengeTypeModal.hide();

    let challengeType: ChallengeType = {
      ChallengeTypeId: 0,
      ChallengeTypeName : (<any>document.getElementById("add_name")).value,
      ParticipantTypeId : (<any>document.getElementById("add_part")).value,
      PointsPerObjective : (<any>document.getElementById("add_points")).value,
    }
    console.log(challengeType);

    this.service.createChallengeType(<JSON><any>challengeType).subscribe(()=>{
      this.loadData();
    });
  }

  deleteChallengeType() {
    this.dangerModal.hide();
    this.editChallengeTypeModal.hide();
    this.service.deleteChallengeType(this.selectedChallengeType.ChallengeTypeId).subscribe(ret => {
      this.loadData();
    });
  }

  ngOnDestroy(): void {

  }

  ngOnInit(): void {
    this.loadData();
  }

  loadData()
  {
    this.isLoading=true;
    this.service.getChallengeTypes().subscribe(
      challengeTypeData => {
        console.log('0');
        this.challengeTypeData = challengeTypeData as ChallengeType[];
        console.log('1');

        if (this.firstLoad) {
          this.dtTrigger.next();
          console.log('2');
        }
        this.firstLoad=false;
        this.isLoading=false;
        console.log('3');
      },
      err => {
        this.isLoading=false;
        console.log(err);
      }
    );
    console.log('stuff');

  }
}
