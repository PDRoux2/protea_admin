import { Component, OnInit, OnDestroy, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { HttpClient } from '@angular/common/http';
import { AppService } from '../../app.service';
import { Subject , Observable } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { Company } from '../../structures';

@Component({
  templateUrl: 'companydetails.component.html'
})
export class CompanyDetailsComponent implements OnDestroy, OnInit {

    constructor(private http: HttpClient, private service: AppService, private location : Location, private company : Company) {

    }

    ngOnDestroy(): void {

    }

    ngOnInit(): void {

    }
}
