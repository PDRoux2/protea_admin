import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

import { CompanyDetailsComponent } from './companydetails.component';
import { DataTablesModule } from 'angular-datatables';
import { CompanyDetailsRoutingModule } from './companydetails-routing.module';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    FormsModule,
    CompanyDetailsRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    CommonModule,
    DataTablesModule
    
  ],
  declarations: [ CompanyDetailsComponent ]
})
export class CompanyDetailsModule { }
