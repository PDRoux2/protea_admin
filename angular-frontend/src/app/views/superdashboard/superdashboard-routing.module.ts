import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuperDashboardComponent } from './superdashboard.component';

const routes: Routes = [
  {
    path: '',
    component: SuperDashboardComponent,
    data: {
      title: 'Charts'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuperDashboardRoutingModule {}
