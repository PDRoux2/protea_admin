import { Component } from '@angular/core';
import { AppService } from '../../app.service';

@Component({
  templateUrl: 'superdashboard.component.html'
})
export class SuperDashboardComponent {

  public startDate = "";
  public endDate = "";
  public aType = "2";
  public displayType = "0";
  public loading = true;

  public dataSub = null;
  public lastData: any;
  public lastTotalData: any;
  public userData: any;

  public countryList = null;
  public languageList = null;
  public divisionList = null;
  public companyList = null;
  public divisionListFilt = null;
  public companyListFilt = null;
  public sessionTypeList = null;

  public companyFilter = -1;
  public languageFilter = -1;
  public divisionFilter = -1;
  public countryFilter = -1;
  public sessionTypeFilter = -1;

  public allSessions = 0;
  public allUsers = 0;

  public fullUserCount = 0;

  public mainChartData = null;
  /* tslint:disable:max-line-length */
  public mainChartLabels = null;
  public mainChartLabelsDisplay = null;

  public Months=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];


  public mainChartOptions: any = {
    tooltips: {
      enabled: false,
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        gridLines: {
          drawOnChartArea: false,
        },
      }],
      yAxes: [{
        ticks: {
          beginAtZero: true,
          maxTicksLimit: 5,
        }
      }]
    },
    elements: {
      line: {
        borderWidth: 2,
        tension: 0,

      },
      point: {
        radius: 5,
        hitRadius: 10,
        hoverRadius: 4,
        hoverBorderWidth: 3,
      }
    },
    legend: {
      display: true
    }
  };

  public mainChartColours: Array<any> = [];
  public basicColours:Array<any>=[
    {
      backgroundColor: 'rgba(38,77,136,.3)',
      borderColor: 'rgba(38,77,136,1)',
    },
    {
      backgroundColor: 'rgba(1,146,96,.3)',
      borderColor: 'rgba(1,146,96,1)',
    },
    {
      backgroundColor: 'rgba(248,180,37,.3)',
      borderColor: 'rgba(248,180,37,1)',
    },
    {
      backgroundColor: 'rgba(119,110,54,.3)',
      borderColor: 'rgba(119,110,54,1)',
    },
    {
      backgroundColor: 'rgba(228,56,132,.3)',
      borderColor: 'rgba(228,56,132,1)',
    },
    {
      backgroundColor: 'rgba(94,57,141,.3)',
      borderColor: 'rgba(94,57,141,1)',
    }
  ];
  public mainChartLegend = true;
  public mainChartType = 'line';

  constructor(private service: AppService) {

  }

  ngOnInit(): void {
    this.service.getCountries().subscribe((c) => this.countryList = c);
    this.service.getLanguages().subscribe((c) => this.languageList = c);
    this.service.getCompanies().subscribe((c) => this.companyListFilt = this.companyList = c);
    this.service.getDivisions().subscribe((d) => this.divisionListFilt = this.divisionList = d);
    this.service.getSessionTypes().subscribe((t) => this.sessionTypeList = t);

    this.startDate = this.mkDate(Date.now() - 120 * 24 * 60 * 60 * 1000);
    this.endDate = this.mkDate(Date.now());
    this.service.getUserCounts().subscribe((data) => {
      console.log(data);
      this.userData = data;
      this.loadData();
    });

  }

  loadData() {
    if (this.dataSub) this.dataSub.unsubscribe();
    this.loading = true;
    this.dataSub = this.service.getSessionData(this.aType, this.startDate, this.endDate).subscribe((data: any) => {
      this.lastData = data.sort((a, b) => (+a.userId - (+b.userId)));
      console.log(data);
      this.filterData();
      this.loading = false;
    });
  }

  checkFilters() {
    this.companyListFilt = this.companyList.filter(row => ((this.countryFilter == -1) || (row.CountryId == this.countryFilter)));
    if (this.companyFilter != -1)
      this.divisionListFilt = this.divisionList.filter(row => (row.CompanyId == this.companyFilter));
    else if (this.countryFilter != -1)
      this.divisionListFilt = this.divisionList.filter(row => {
        let com = this.companyList.find(e => (e.CompanyId == row.CompanyId))
        return com.CountryId == this.countryFilter;
      });
    else
      this.divisionListFilt = this.divisionList;
    this.filterData();
  }

  filterData() {
    let isByST = (this.displayType == '1') || (this.displayType == '11');
    let filt = this.lastData.filter(row => {
      if ((this.countryFilter != -1) && (row.CountryId != this.countryFilter)) return false;
      if ((this.companyFilter != -1) && (row.CompanyId != this.companyFilter)) return false;
      if ((this.languageFilter != -1) && (row.LanguageId != this.languageFilter)) return false;
      if ((this.divisionFilter != -1) && (row.DivisionId != this.divisionFilter)) return false;
      if ((this.sessionTypeFilter != -1) && (row.SessionTypeId != this.sessionTypeFilter)) return false;
      return true;
    });

    //figure out the possible series
    this.mainChartData = [];
    this.mainChartLabels = [];
    this.mainChartLabelsDisplay = [];

    if (filt.length == 0) {
      this.mainChartData.push({ //just add a blank series
        label: this.getSeriesName(null),
        colours: this.getSeriesColour(null),
        data: [],
        lastUID: [],
      });
    } else //we have some data so
      filt.forEach(row => {
        let sn = this.getSeriesName(row);
        let e = this.mainChartData.find(el => el.label == sn);
        if (!e) //no such series series yet
          this.mainChartData.push({
            label: sn,
            colours: this.getSeriesColour(row),
            data: [],
            lastUID: [],
          })
      });

    this.allSessions = 0;
    this.allUsers = 0;

    //set up the colours
    this.mainChartColours=this.mainChartData.map(d=>d.colours);

    //establish a list of all dates
    for (var d = new Date(Date.parse(this.startDate)); d <= new Date(Date.parse(this.endDate)); d.setDate(d.getDate() + 1)) {
      let dm = new Date(d);
      let last = new Date(d);
      let extra = '';
      if (this.aType == "2") dm = new Date(d.getFullYear(), d.getMonth(), 2);
      if (this.aType == "1") {
        let day = dm.getDay();
        let diff = dm.getDate() - day + (day == 0 ? -6 : 1);
        dm = new Date(dm.setDate(diff));
        last = new Date(last.setDate(diff + 6));
        extra = ' - ' + last.toISOString().substring(5, 10).replace(/-/g, "/")
      }
      let ds = dm.toISOString().substring(0, 10).replace(/-/g, "/") + extra;
      let dsd= ds;
      if (this.aType=='2') //reformat date for this case
        dsd=this.Months[dm.getMonth()]+' '+dm.getFullYear();


      //force these Dates
      let pos = this.mainChartLabels.findIndex(el => (el == ds));
      if (pos < 0) {
        pos = this.mainChartLabels.length;
        this.mainChartLabels.push(ds);
        this.mainChartLabelsDisplay.push(dsd);

        this.mainChartData.forEach(d => {
          d.data[pos] = 0;
          d.lastUID[pos] = 0;
        });
      }
    }

    let isVal = !(Math.floor((+this.displayType) / 10));


    //now add the total user data
    let ufilt = this.userData.filter(row => {
      if ((this.countryFilter != -1) && (row.CountryId != this.countryFilter)) return false;
      if ((this.companyFilter != -1) && (row.CompanyId != this.companyFilter)) return false;
      if ((this.languageFilter != -1) && (row.LanguageId != this.languageFilter)) return false;
      if ((this.divisionFilter != -1) && (row.DivisionId != this.divisionFilter)) return false;
      return true;
    });
    this.fullUserCount = ufilt.reduce((val, row) => val + row.UserCount, 0);
    //calculate the totals for each date in the range
    let allUsersByTime=this.mainChartLabels.map(d=>{
      let count=ufilt.reduce((val, row) => {
        if (row.DateJoined.substring(0,10).replace(/-/g,'/')<d.substring(0,10))
          return val + row.UserCount;
        else return val;
       }, 0);
       return count;
    });
    console.log("Time all user break down");
    console.log(this.mainChartLabels);
    console.log(allUsersByTime);

    this.allSessions = 0;
    this.allUsers = 0;
    let lastUID = 0;

    filt.forEach(row => {
      let sn = this.getSeriesName(row);
      let series = this.mainChartData.find(el => (el.label == sn));
      let pos = this.mainChartLabels.findIndex(el => (el.substring(0, 10) == row.Date.substring(0, 10)));
      if (pos < 0) return false; //we are really supposed to have this date

      if (isVal) series.data[pos] += row.userSessions; //add sessions
      else series.data[pos] += (series.lastUID[pos] == row.userId) ? 0 : 1; //count a user if not a duplicate
      series.lastUID[pos] = row.userId;

      this.allSessions += row.userSessions; //sum all sessions
      this.allUsers += (lastUID == row.userId) ? 0 : 1; //count user if not a duplicate

      lastUID = row.userId;
    });

    //so divide out if the type is active percentage
    if (this.displayType=='20') {
      let series=this.mainChartData[0];
      console.log(series.data);
      this.mainChartLabels.forEach((l,i)=>{
        console.log(i,series.data);
        series.data[i]*=100/allUsersByTime[i];
      });
      console.log(series.data);
    }





  }

  getSeriesName(row) {

    if (this.displayType == "0") return $localize`:@@graphseries_sessions:Sessions`;
    if (this.displayType == "10") return $localize`:@@graphseries_users:Users`;
    if (this.displayType == "20") return $localize`:@@graphseries_activeuserspercent:Active User Percent`;

    if (!row) return "No Data";

    let val = (+this.displayType) % 10;

    if (val == 1) {
      let type = this.sessionTypeList.find(st => st.SessionTypeId == row.SessionTypeId);
      return type ? type.SessionTypeName : "UNKNOWN";
    }
    if (val == 2) {
      let div = this.divisionList.find(st => st.DivisionId == row.DivisionId);
      return div ? div.DivisionName : 'UNKNOWN';
    }
  }

  getSeriesColour(row) {
    if ((!row)||(this.displayType == "0")||(this.displayType == "10")||(this.displayType == "20")) return this.basicColours[0];

    let val = (+this.displayType) % 10;
    if (val == 1) {
      let type = this.sessionTypeList.findIndex(st => st.SessionTypeId == row.SessionTypeId);
      return (type>=0) ? this.basicColours[type] : this.basicColours[0];
    }
    if (val == 2) {
      let div = this.divisionListFilt.findIndex(st => st.DivisionId == row.DivisionId);
      return (div>=0)? this.basicColours[div] : this.basicColours[0];
    }
    return this.basicColours[0];
  }


  mkDate(dateMilli) {
    return new Date(dateMilli).toISOString().substring(0, 10)
  }

}
