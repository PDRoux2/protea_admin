import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ChartsModule } from 'ng2-charts';

import { SuperDashboardComponent } from './superdashboard.component';
import { SuperDashboardRoutingModule } from './superdashboard-routing.module';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    FormsModule,
    SuperDashboardRoutingModule,
    ChartsModule,
    NgbModule,
    CommonModule,
  ],
  declarations: [ SuperDashboardComponent ]
})
export class SuperDashboardModule { }
