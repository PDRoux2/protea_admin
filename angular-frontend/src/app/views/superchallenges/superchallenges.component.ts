import { Component, OnInit, OnDestroy, ViewChildren, QueryList, ViewChild, Renderer2, ComponentRef, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { AppService } from '../../app.service';
import { DataTableDirective } from 'angular-datatables';
import { Challenge, Company, Division, Country, ChallengeAvaliability, ChallengeType, ChallengeTeam, Participant, User } from '../../structures';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  templateUrl: 'superchallenges.component.html'
})
export class SuperChallengesComponent implements OnDestroy, OnInit {

  challengeData: Challenge[];

  companyData: Company[];
  fCompanies: Company[];
  companyLookup = {};



  divisionData: Division[];
  divisionLookup = {};
  divisionNames = {};

  countryData: Country[];
  countryNames = {};


  selectedChallenge: Challenge;
  teamChallenge: Challenge;
  detailedChallenge: number = 0;
  isWorking=false;


  teamData: ChallengeTeam[] = [];
  teamLoading = false;


  participantData: Participant[] = [];

  avaliableUsers: User[] = [];
  teamUsers: User[] = [];

  avaliableCountries: number[] = [];
  avaliableDivisions: number[] = [];
  isLoading = false;

  @ViewChildren(DataTableDirective) dtElements: QueryList<DataTableDirective>;
  @ViewChild('addChallengeModal') public addChallengeModal: ModalDirective;
  @ViewChild('editChallengeModal') public editChallengeModal: ModalDirective;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  @ViewChild('teamModal') public teamModal: ModalDirective;

  dtOptions: DataTables.Settings = {
    "columnDefs": [{
      "targets": 0,
      "orderable": false
    }]
  };



  constructor(private http: HttpClient, private service: AppService) {

  }

  closeAddModal() {
    this.addChallengeModal.hide();
    (<any>document.getElementById("add_scroll")).scrollTop=0;
    (<any>document.getElementById("add_name")).value = "";
    (<any>document.getElementById("add_desc")).value = "Joining a challenge will allow administration to see your involvement in this challenge";
    (<any>document.getElementById("add_start_date")).value = "";
    (<any>document.getElementById("add_end_date")).value = "";
    if (document.getElementById("add_goal"))
      (<any>document.getElementById("add_goal")).value = "";
  }

  closeEditModal() {
    this.editChallengeModal.hide();
    (<any>document.getElementById("edit_name")).value = "";
    (<any>document.getElementById("edit_start_date")).value = "";
    (<any>document.getElementById("edit_end_date")).value = "";
    if (document.getElementById("edit_goal"))
      (<any>document.getElementById("edit_goal")).value = "";
  }

  startAddChallenge()
  {
    (<any>document.getElementById("add_desc")).value = "Joining a challenge will allow administrators to see your involvement in this challenge";
    (<any>document.getElementById("add_country_select")).value = this.companyData[0].CountryId;
    this.countryChange(this.companyData[0].CountryId);
    setTimeout(()=>{
      if (this.companyData.length==1) //company user
        (<any>document.getElementById("add_company_select")).value = this.companyData[0].CompanyId;

      this.addChallengeModal.show();
    },10);
  }

  addChallenge() {

    let isMax=(<any>document.getElementById("add_goal_type")).value=="0";


    let challenge: Challenge = {
      ChallengeId: 0,
      ChallengeName: (<any>document.getElementById("add_name")).value,
      Description: (<any>document.getElementById("add_desc")).value,
      ChallengeTypeId: this.makeChallengeType(+(<any>document.getElementById("add_type_select")).value,+(<any>document.getElementById("add_goal_type")).value),
      DailyGoal: isMax?0:(<any>document.getElementById("add_goal")).value,
      StartDate: (<any>document.getElementById("add_start_date")).value,
      EndDate: (<any>document.getElementById("add_end_date")).value
    };


    if ((!isMax)&&((challenge.DailyGoal<1)||(challenge.DailyGoal>9)||(isNaN(challenge.DailyGoal)))) {
      alert("Daily Goal must be a number between 1 and 9");
      return;
    }

    let companyId=+(<any>document.getElementById("add_company_select")).value;
    let countryId=+(<any>document.getElementById("add_country_select")).value;
    this.isWorking=true;
    this.service.createChallenge(<JSON><any>challenge).subscribe(cha => {
      //got a good result lets do some other updates
      (<any>this.selectedChallenge)=cha;
      console.log("saving...");
      console.log(this.selectedChallenge,countryId,companyId);

      if (countryId && (!companyId)) {
        console.log("creating for countryid="+countryId)
        this.service.addCountryFromChallenge(this.selectedChallenge.ChallengeId,countryId).subscribe(()=>{});
      }
      if (companyId) {//Add new divisions
        console.log("saving company")
        this.divisionData.filter((d)=>(d.CompanyId==companyId))
            .forEach((d)=> {
              this.service.addDivisionFromChallenge(this.selectedChallenge.ChallengeId,d.DivisionId).subscribe(()=>{});
            });
      }
      setTimeout(()=>{
        this.loadChallenges()
        this.closeAddModal();
        this.isWorking=false;
        if (this.isTeam(challenge.ChallengeTypeId)) //is a team challenge
           this.editChallengeTeam(this.selectedChallenge);
      },1000)
    });
  }

  isTeam(ctid) {
    return ((ctid==2)||(ctid==4))
  }

  isLocked(c)
  {    
    return (+new Date())>(+new Date(c.EndDate));
  }

  isMax(ctid) {
    return ((ctid==3)||(ctid==4))
  }

  makeChallengeType(isTeam,isDaily) {
    if (isTeam) return isDaily?2:4;
    else return isDaily?1:3;
  }


  countryChange(countryId) {
    this.fCompanies=this.companyData.filter(c=>c.CountryId==countryId)
  }

  deleteChallenge() {
    this.dangerModal.hide();
    this.closeEditModal();
    this.service.deleteChallenge(this.selectedChallenge.ChallengeId).subscribe(repon => {
      this.loadChallenges();
    });
  }

  allowFEdits=false;

  editChallenge(challenge: Challenge) {

    this.selectedChallenge = challenge;

    //check if we have members
    this.allowFEdits=true;
    this.service.getChallengeWinners(challenge.ChallengeId,challenge.isTeam).subscribe(
      data=>{
        console.log(data);
        if ((!data)||(!data.length)) return; //no data so all good
        if (!challenge.isTeam) { //we have a memeber
          this.allowFEdits=false;
        } else {
          this.allowFEdits=!data.find(v=>(v.UserCount>0));//check for any team with a member
        }
        console.log(this.allowFEdits);
      });

    this.service.getChallengeAvaliability(challenge.ChallengeId).subscribe(
      challengeData => {
        challenge.Avliability = challengeData as ChallengeAvaliability[];
        console.log(challenge.Avliability);
        challenge.CountryId=challenge.CompanyId=0;
        challenge.Avliability.forEach(element => {
          console.log(element);
          if (element.CountryId != null && element.CountryId != 0) {
            challenge.CountryId=element.CountryId;
            this.countryChange(element.CountryId);
          }
          if (element.DivisionId != null && element.DivisionId != 0) {
            let division=this.divisionLookup[element.DivisionId];
            if (division) {
              challenge.CountryId=division.CountryId;
              challenge.CompanyId=division.CompanyId;
              this.countryChange(division.CountryId);
            }
          }
          console.log(challenge);
        });
        //wait for angular to have updated the selects
        setTimeout(()=>{
          (<any>document.getElementById("edit_name")).value = challenge.ChallengeName;
          (<any>document.getElementById("edit_desc")).value = challenge.Description;
          (<any>document.getElementById("edit_start_date")).value = challenge.StartDate.substring(0,10);
          (<any>document.getElementById("edit_end_date")).value = challenge.EndDate.substring(0,10);;
          (<any>document.getElementById("edit_goal_type")).value = this.isMax(challenge.ChallengeTypeId)?"0":"1";
          (<any>document.getElementById("edit_goal")).value = challenge.DailyGoal;
          (<any>document.getElementById("edit_type_select")).value = this.isTeam(challenge.ChallengeTypeId)?"1":"0";
          (<any>document.getElementById("edit_country_select")).value = challenge.CountryId;
          (<any>document.getElementById("edit_company_select")).value = challenge.CompanyId;
          this.editChallengeModal.show();
        },10);

      },
      err => {
        console.log(err);
      }
    );

  }



  updateChallenge() {
    let isMax=(<any>document.getElementById("edit_goal_type")).value=="0";
    let challenge: Challenge = {
      ChallengeId: this.selectedChallenge.ChallengeId,
      ChallengeName: (<any>document.getElementById("edit_name")).value,
      Description: (<any>document.getElementById("edit_desc")).value,
      ChallengeTypeId: this.makeChallengeType(+(<any>document.getElementById("edit_type_select")).value,+(<any>document.getElementById("edit_goal_type")).value),
      DailyGoal: isMax?0:(<any>document.getElementById("edit_goal")).value,
      StartDate: (<any>document.getElementById("edit_start_date")).value,
      EndDate: (<any>document.getElementById("edit_end_date")).value
    };

    if ((!isMax)&&((challenge.DailyGoal<1)||(challenge.DailyGoal>9)||(isNaN(challenge.DailyGoal)))) {
      alert("Daily Goal must be a number between 1 and 9");
      return;
    }

    let companyId=+(<any>document.getElementById("edit_company_select")).value;
    let countryId=+(<any>document.getElementById("edit_country_select")).value;
    this.isWorking=true;
    this.service.updateChallenge(<JSON><any>challenge).subscribe(cha => {
      //got a good result lets do some other updates
      if ((this.selectedChallenge.CountryId!=countryId)||(companyId)) {
        if (this.selectedChallenge.CountryId) //was an old one
          this.service.removeCountryFromChallenge(this.selectedChallenge.ChallengeId,this.selectedChallenge.CountryId).subscribe(()=>{});
        if (countryId) //there is a new one
          this.service.addCountryFromChallenge(this.selectedChallenge.ChallengeId,countryId).subscribe(()=>{});
      }
      if (this.selectedChallenge.CompanyId!=companyId) {
        if (this.selectedChallenge.CompanyId) {//remove old Divisions
          this.divisionData.filter((d)=>(d.CompanyId==this.selectedChallenge.CompanyId))
              .forEach((d)=> {
                this.service.removeDivisionFromChallenge(this.selectedChallenge.ChallengeId,d.DivisionId).subscribe(()=>{});
              });
        }
        if (companyId) {//add new Divisions
          this.divisionData.filter((d)=>(d.CompanyId==companyId))
              .forEach((d)=> {
                this.service.addDivisionFromChallenge(this.selectedChallenge.ChallengeId,d.DivisionId).subscribe(()=>{});
              });
        }
      }
      this.loadChallenges();
      this.closeEditModal();
      this.isWorking=false;
      if (this.isTeam(challenge.ChallengeTypeId)) //is a team challenge
         this.editChallengeTeam(this.selectedChallenge);
    });
  }

  editChallengeTeam(challenge: Challenge) {
    this.teamLoading = true;
    this.teamChallenge=challenge
    this.service.getChallengeTeams(challenge.ChallengeId).subscribe(
      teamData => {
        this.teamData = teamData;
        this.teamLoading = false;
      });
    this.teamModal.show();
  }


  closeTeamModal() {
    this.teamModal.hide();
  }

  addTeam() {
    this.teamData.push({
      TeamId: 0,
      ChallengeId: this.teamChallenge.ChallengeId,
      TeamName: ''
    });
  }

  saveTeams() {
    this.teamData.forEach((v,i)=>{
      let name=(<any>document.getElementById("team_name_"+i)).value; //get new descripiton
      if (v.TeamName==name) return; //hasn't changed
      v.TeamName=name;
      if (v.TeamId)
        this.service.updateTeam(<JSON><any>v).subscribe((ret)=>{})
      else
        this.service.createTeam(<JSON><any>v).subscribe((ret)=>{})
    });
    this.teamModal.hide();
  }


  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event!!
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.service.getCountries().subscribe(
      countryData => {
        this.countryData = countryData as Country[];
        this.countryData.forEach(element => {
          this.countryNames[element.CountryId] = element.CountryName;
        });
        this.service.getCompanies().subscribe(
          companyData => {
            this.companyData = companyData as Company[];
            companyData.forEach(element => {
              this.companyLookup[element.CompanyId] = element;
            })
            this.service.getDivisions().subscribe(
              divisionData => {
                divisionData.forEach(element => {
                  this.divisionNames[element.DivisionId] = element.DivisionName;
                  this.divisionLookup[element.DivisionId] = element;
                  element.CountryId = this.companyLookup[element.CompanyId].CountryId;
                })
                this.divisionData = divisionData as Division[];
                this.loadChallenges();
              },
              err => {
                console.log(err);
              }
            );
          },
          err => {
            console.log(err);
          }
        );
      },
      err => {
        console.log(err);
      }
    );
  }

  loadChallenges() {
    this.isLoading=true;
    this.service.getChallenges().subscribe(
      challengeData => {
        console.log(challengeData);
        //this.challengeData = (challengeData as Challenge[]);
        this.challengeData=(challengeData as Challenge[]).sort((a,b) => (a.StartDate>b.StartDate)?-1:1);

        this.isLoading=false;

      },
      err => {
        console.log(err);
      });
  }

}
