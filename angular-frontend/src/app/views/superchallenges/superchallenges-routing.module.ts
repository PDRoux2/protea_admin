import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuperChallengesComponent } from './superchallenges.component';

const routes: Routes = [
  {
    path: '',
    component: SuperChallengesComponent,
    data: {
      title: 'Dashboard'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuperChallengesRoutingModule {}
