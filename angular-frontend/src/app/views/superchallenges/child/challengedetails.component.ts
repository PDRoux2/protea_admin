import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './challengedetails.component.html',
})
export class ChildComponent implements OnInit {

  dtOptions: DataTables.Settings = {};

  @Input() data: any[];

  constructor() { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 2,
      processing: true
    }
  }

}