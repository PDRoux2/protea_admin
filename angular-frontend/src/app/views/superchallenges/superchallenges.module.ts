import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { ModalModule } from 'ngx-bootstrap/modal';


import { ChildComponent } from './child/challengedetails.component';
import { SuperChallengesComponent } from './superchallenges.component';
import { DataTablesModule } from 'angular-datatables';
import { SuperChallengesRoutingModule } from './superchallenges-routing.module';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    FormsModule,
    SuperChallengesRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    CommonModule,
    DataTablesModule,
    ModalModule.forRoot(),
    NgbModule,
  ],
  declarations: [ SuperChallengesComponent, ChildComponent ],
  providers: [
  ],
  bootstrap: [SuperChallengesComponent],
  entryComponents: [ChildComponent]
})
export class SuperChallengesModule { }
