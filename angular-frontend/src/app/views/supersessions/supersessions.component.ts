import { Component, OnInit, OnDestroy, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { HttpClient } from '@angular/common/http';
import { AppService } from '../../app.service';
import { Subject , Observable } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { Session, Company } from '../../structures';
import {ModalDirective} from 'ngx-bootstrap/modal';

@Component({
  templateUrl: 'supersessions.component.html'
})
export class SuperSessionsComponent implements OnDestroy, OnInit {

    sessionData : Session[];

    @ViewChild(DataTableDirective) dtElement: DataTableDirective;
    @ViewChild('addSessionModal') public addSessionModal: ModalDirective;

    selectedSession : Session;

    dtOptions: DataTables.Settings = {
      "columnDefs": [ {
      "targets": 0,
      "orderable": false
      } ]};

    dtTrigger: Subject<any> = new Subject();


    constructor(private service: AppService) {

    }

    postSession(){
      this.addSessionModal.hide();
      let session = {};
      session["UserId"] = (<any>document.getElementById("add_user")).value;
      session["DateAndTime"] = (<any>document.getElementById("add_date")).value;
      session["SessionTypeId"] = (<any>document.getElementById("add_sessiontype")).value;

      this.service.createSession(<JSON><any>session).subscribe(div => {
        this.sessionData.push(<Session><any>div);
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.dtTrigger.next();
        });
      });
    }

    ngOnDestroy(): void {
      // Do not forget to unsubscribe the event
      this.dtTrigger.unsubscribe();
    }

    ngOnInit(): void {
      this.service.getSessions().subscribe(
        sessionData =>  {
          this.sessionData = sessionData as Session[];
          this.dtTrigger.next();
      },
      err => {
        console.log(err);
      }
      );
    }
}
