import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuperSessionsComponent } from './supersessions.component';

const routes: Routes = [
  {
    path: '',
    component: SuperSessionsComponent,
    data: {
      title: 'Dashboard'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuperSessionsRoutingModule {}
