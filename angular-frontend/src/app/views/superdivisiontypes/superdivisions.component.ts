import { Component, OnInit, OnDestroy, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { HttpClient } from '@angular/common/http';
import { AppService } from '../../app.service';
import { Subject , Observable } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { Division, Company } from '../../structures';
import {ModalDirective} from 'ngx-bootstrap/modal';

@Component({
  templateUrl: 'superdivisions.component.html'
})
export class SuperDivisionsComponent implements OnDestroy, OnInit {

    divisionData : Division[];
    companyData : Company[];
    companyNames = {};

    @ViewChild(DataTableDirective) dtElement: DataTableDirective;
    @ViewChild('addDivisionModal') public addDivisionModal: ModalDirective;
    @ViewChild('editDivisionModal') public editDivisionModal: ModalDirective;
    @ViewChild('dangerModal') public dangerModal: ModalDirective;

    selectedDivision : Division;

    dtOptions: DataTables.Settings = {
      "columnDefs": [ {
      "targets": 0,
      "orderable": false
      } ]};

    dtTrigger: Subject<any> = new Subject();


    constructor(private http: HttpClient, private service: AppService) {

    }

    editDivision(division : Division){
      this.selectedDivision = division;

      (<any>document.getElementById("edit_code")).value = division.DivisionCode;
      (<any>document.getElementById("edit_name")).value = division.DivisionName;
      (<any>document.getElementById("edit_company")).value = division.CompanyId;

      this.editDivisionModal.show();
    }

    revealCompany(s : string){
      document.getElementById("details").hidden = false;
      document.getElementById("details_name").innerText = s;
    }

    postDivision(){
      this.addDivisionModal.hide();
      let division = {};
      division["DivisionCode"] = (<any>document.getElementById("add_name")).value;
      division["DivisionName"] = (<any>document.getElementById("add_code")).value;
      division["CompanyId"] = (<any>document.getElementById("add_company")).value;

      this.service.createDivision(<JSON><any>division).subscribe(div => {
        this.divisionData.push(<Division><any>div);
        div["CompanyName"] = this.companyNames[div["CompanyId"]];
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.dtTrigger.next();
        });
      });
    }

    deleteDivision(){
      this.dangerModal.hide();
      this.editDivisionModal.hide();
      this.service.deleteDivision(this.selectedDivision.DivisionId).subscribe(user => {
      let index = this.divisionData.indexOf(this.selectedDivision);
        if (index !== -1) {
          this.divisionData.splice(index, 1);
        }
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.dtTrigger.next();
        });
      });
    }

    updateDivision(){
      this.editDivisionModal.hide();

      let division : Division = this.selectedDivision;
      
      division.DivisionCode = (<any>document.getElementById("edit_code")).value;
      division.DivisionName = (<any>document.getElementById("edit_name")).value;
      division.CompanyId = (<any>document.getElementById("edit_company")).value;

      this.service.updateUser(<JSON><any>division).subscribe();
    }

    ngOnDestroy(): void {
      // Do not forget to unsubscribe the event
      this.dtTrigger.unsubscribe();
    }

    ngOnInit(): void {
      this.service.getCompanies().subscribe(
        companyData =>  {
          this.companyData = companyData;
          (<Company[]><any>companyData).forEach(element => {
            this.companyNames[element["CompanyId"]] = element["CompanyName"];
          });
          this.service.getDivisions().subscribe(
            divisionData =>  {
              this.divisionData = divisionData as Division[];
              this.divisionData.forEach(element => {
                console.log(this.companyNames[element["CompanyId"]]);
                element["CompanyName"] = this.companyNames[element["CompanyId"]];
              });
              this.dtTrigger.next();
          },
          err => {
            console.log(err);
          }
          );
        },
        err => {
          console.log(err);
        }
      );
    }
}
