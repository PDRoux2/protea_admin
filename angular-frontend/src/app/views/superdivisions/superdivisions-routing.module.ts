import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuperDivisionsComponent } from './superdivisions.component';

const routes: Routes = [
  {
    path: '',
    component: SuperDivisionsComponent,
    data: {
      title: 'Dashboard'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuperDivisionsRoutingModule {}
