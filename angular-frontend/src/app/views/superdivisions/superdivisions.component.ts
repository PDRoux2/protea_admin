import { Component, OnInit, OnDestroy, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { HttpClient } from '@angular/common/http';
import { AppService } from '../../app.service';
import { Subject, Observable } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { Division, Company } from '../../structures';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  templateUrl: 'superdivisions.component.html'
})
export class SuperDivisionsComponent implements OnDestroy, OnInit {

  divisionData: Division[];
  companyData: Company[];
  companyNames = {};

  @ViewChild(DataTableDirective) dtElement: DataTableDirective;
  @ViewChild('addDivisionModal') public addDivisionModal: ModalDirective;
  @ViewChild('editDivisionModal') public editDivisionModal: ModalDirective;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;

  selectedDivision: Division;
  isLoading = false;

  dtOptions: DataTables.Settings = {
    "columnDefs": [{
      "targets": 0,
      "orderable": false
    }]
  };

  dtTrigger: Subject<any> = new Subject();


  constructor(private http: HttpClient, private service: AppService) {

  }

  editDivision(division: Division) {
    this.selectedDivision = division;

    (<any>document.getElementById("edit_code")).value = division.DivisionCode;
    (<any>document.getElementById("edit_name")).value = division.DivisionName;
    (<any>document.getElementById("edit_company")).value = division.CompanyId;

    this.editDivisionModal.show();
  }

  revealCompany(s: string) {
    document.getElementById("details").hidden = false;
    document.getElementById("details_name").innerText = s;
  }

  postDivision() {
    let Code = (<any>document.getElementById("add_code")).value;
    this.service.checkDivision(-1, Code).subscribe((response) => {
      console.log("Check division on " + Code + " returns " + response);
      if (response) alert("This division code already exists on a different division, must use a unique code.")
      else {
        let division = {};
        division["DivisionCode"] = (<any>document.getElementById("add_code")).value;
        division["DivisionName"] = (<any>document.getElementById("add_name")).value;
        division["CompanyId"] = (<any>document.getElementById("add_company")).value;
        if (!this.checkDivisionDescUnique(division)) return;
        this.addDivisionModal.hide();

        this.service.createDivision(<JSON><any>division).subscribe(div => {
          this.divisionData.push(<Division><any>div);
          div["CompanyName"] = this.companyNames[div["CompanyId"]];
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          });
        });
      }

    });

  }

  deleteDivision() {
    this.dangerModal.hide();
    this.editDivisionModal.hide();
    this.service.deleteDivision(this.selectedDivision.DivisionId).subscribe(user => {
      let index = this.divisionData.indexOf(this.selectedDivision);
      if (index !== -1) {
        this.divisionData.splice(index, 1);
      }
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
        this.dtTrigger.next();
      });
    }, (err) => {
      console.log(err);
      alert('Could not delete division, one of more users are still associated with this division.')
    });
  }

  updateDivision() {

    let Code = (<any>document.getElementById("edit_code")).value;

    this.service.checkDivision(this.selectedDivision.DivisionId, Code).subscribe((response) => {
      console.log("Check division on " + Code + " returns " + response)
      if (response) alert("This division code already exists on a different division, must use a unique code.")
      else {
        let division: Division = this.selectedDivision;

        division.DivisionCode = (<any>document.getElementById("edit_code")).value;
        division.DivisionName = (<any>document.getElementById("edit_name")).value;
        division.CompanyId = (<any>document.getElementById("edit_company")).value;
        if (!this.checkDivisionDescUnique(division)) return;
        division["CompanyName"] = this.companyNames[division.CompanyId];
        this.editDivisionModal.hide();
        this.service.updateDivision(<JSON><any>division).subscribe(() => console.log('done'), (err) => console.log('error', err));
      }
    });


  }

  checkDivisionDescUnique(nd) {
    let match = this.divisionData.find(d => ((nd.DivisionId != d.DivisionId) && (nd.DivisionName == d.DivisionName) && (nd.CompanyId == d.CompanyId)));

    if (match) alert("A division with this name for this company already exists.")
    return !match;
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.service.getCompanies().subscribe(
      companyData => {
        this.companyData = companyData;
        (<Company[]><any>companyData).forEach(element => {
          this.companyNames[element["CompanyId"]] = element["CompanyName"];
        });
        this.service.getDivisions().subscribe(
          divisionData => {
            this.divisionData = divisionData as Division[];
            this.divisionData.forEach(element => {
              console.log(this.companyNames[element["CompanyId"]]);
              element["CompanyName"] = this.companyNames[element["CompanyId"]];
            });
            this.isLoading = false;
            this.dtTrigger.next();
          },
          err => {
            console.log(err);
          }
        );
      },
      err => {
        console.log(err);
      }
    );
  }
}
