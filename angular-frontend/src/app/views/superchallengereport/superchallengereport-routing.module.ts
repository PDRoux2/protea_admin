import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuperChallengeReportComponent } from './superchallengereport.component';

const routes: Routes = [
  {
    path: '',
    component: SuperChallengeReportComponent,
    data: {
      title: 'Challenge Report'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuperChallengeReportRoutingModule {}
