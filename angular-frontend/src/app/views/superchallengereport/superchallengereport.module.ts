import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ChartsModule } from 'ng2-charts';

import { SuperChallengeReportComponent } from './superchallengereport.component';
import { SuperChallengeReportRoutingModule } from './superchallengereport-routing.module';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    FormsModule,
    SuperChallengeReportRoutingModule,
    ChartsModule,
    NgbModule,
    CommonModule,
  ],
  declarations: [ SuperChallengeReportComponent ]
})
export class SuperChallengeReportModule { }
