import { Component } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartTooltipLabelColor } from 'chart.js';
import * as moment from 'moment';

import { AppService } from '../../app.service';
import { ChallengeReport, ChallengeReportSlice, ChallengeReportSliceWithChallengeInfo } from '../../structures';

enum BucketType {
    Day = "Day",
    Week = "Week",
    Month = "Month",
}

@Component({
  templateUrl: 'superchallengereport.component.html'
})
export class SuperChallengeReportComponent {
    public startDate = "";
    public endDate = "";
    public bucketType: BucketType = BucketType.Month;
    public loading = true;
    public hasData = true;
    public zeroUsers = false;

    public countryList = null;
    public divisionList = null;
    public companyList = null;
    public companyListFilt = null;
    public divisionListFilt = null;

    public companyFilter = -1;
    public divisionFilter = -1;
    public countryFilter = -1;

    // chart fields
    public ChartData: ChartDataSets[] = null;
    public ChartLabels: string[] = null;

    public mainChartOptions: ChartOptions = {
        tooltips: {
            enabled: false,
        },
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            xAxes: [{
                gridLines: {
                    drawOnChartArea: false,
                },
            }],
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    maxTicksLimit: 5,
                }
            }]
        },
        elements: {
            line: {
                borderWidth: 2,
                tension: 0,

            },
            point: {
                radius: 5,
                hitRadius: 10,
                hoverRadius: 4,
                hoverBorderWidth: 3,
            }
        },
        legend: {
            display: true
        }
    };
    public mainChartColours: ChartTooltipLabelColor[] = [
        {
            backgroundColor: 'rgba(38,77,136,.1)',
            borderColor: 'rgba(38,77,136,1)',
        },
        {
            backgroundColor: 'rgba(1,146,96,.1)',
            borderColor: 'rgba(1,146,96,1)',
        },
        {
            backgroundColor: 'rgba(248,180,37,.1)',
            borderColor: 'rgba(248,180,37,1)',
        },
        {
            backgroundColor: 'rgba(228,56,132,.3)',
            borderColor: 'rgba(228,56,132,1)',
        }
    ];
    public mainChartLegend = true;
    public mainChartType = 'line';

    // fields from the API
    public challengeReportData: ChallengeReport = null;
    public chartBuckets: Date[];

    public startingUserCountsFiltered: ChallengeReportSlice[];
    public startingCountUsersInChallengesFiltered: ChallengeReportSliceWithChallengeInfo[];
    public totalUsersFiltered: ChallengeReportSlice[];
    public activeUsersFiltered: ChallengeReportSlice[];
    public usersInChallengesFiltered: ChallengeReportSliceWithChallengeInfo[];
    public activeUsersInChallengesFiltered: ChallengeReportSlice[];

    constructor(private service: AppService) {

    }

    ngOnInit(): void {
        this.service.getCountries().subscribe((c)=> this.countryList=c);
        this.service.getCompanies().subscribe((c)=> this.companyListFilt=this.companyList=c);
        this.service.getDivisions().subscribe((d)=> this.divisionListFilt=this.divisionList=d);

        this.startDate = moment(Date.now()).subtract(6, "months").format('YYYY-MM-DD')
        this.endDate = moment(Date.now()).format('YYYY-MM-DD')
        setTimeout(() => this.loadData(), 3000);
    }

    loadData() {
        this.loading = true;
        console.log("Calling Challenge data with type "+ this.bucketType +" "+this.startDate+" to "+this.endDate);

        this.service.getSessionChallengeData(this.getBucketTypeIndex(this.bucketType), this.startDate, this.endDate).subscribe(response => {
            this.challengeReportData = response;
            this.chartBuckets = this.challengeReportData.Buckets;
            this.filterData();
            this.prepareData();
            this.loading = false;
        });
    }

    private getBucketTypeIndex(bucketType: BucketType): number {
        switch (bucketType) {
            case BucketType.Day: return 0;
            case BucketType.Week: return 1;
            case BucketType.Month: return 2;
        }
    }

    checkFilters() {
        this.companyListFilt = this.companyList.filter(row => ((this.countryFilter == -1) || (row.CountryId == this.countryFilter)));
        if (this.companyFilter != -1)
            this.divisionListFilt = this.divisionList.filter(row => (row.CompanyId == this.companyFilter));
        else if (this.countryFilter != -1)
            this.divisionListFilt = this.divisionList.filter(row => {
                let com = this.companyList.find(e => (e.CompanyId == row.CompanyId))
                return com.CountryId == this.countryFilter;
            });
        else
            this.divisionListFilt = this.divisionList;

        this.filterData();
        this.prepareData();
    }

    private filterData() {
        this.startingUserCountsFiltered = this.applyFilters(this.challengeReportData.StartingUserCounts);
        this.startingCountUsersInChallengesFiltered = this.applyFiltersWithChallengeInfo(this.challengeReportData.StartingCountUsersInChallenges);
        this.totalUsersFiltered = this.applyFilters(this.challengeReportData.TotalUsers);
        this.activeUsersFiltered = this.applyFilters(this.challengeReportData.ActiveUsers);
        this.usersInChallengesFiltered = this.applyFiltersWithChallengeInfo(this.challengeReportData.UsersInChallenges);
        this.activeUsersInChallengesFiltered = this.applyFilters(this.challengeReportData.ActiveUsersInChallenges);
    }

    private applyFilters(slice: ChallengeReportSlice[]): ChallengeReportSlice[] {
        let filteredSlice = slice.filter(row => {
            if (!row) return false;
            if ((this.countryFilter != -1) && (row.CountryId != this.countryFilter)) return false;
            if ((this.companyFilter != -1) && (row.CompanyId != this.companyFilter)) return false;
            if ((this.divisionFilter != -1) && (row.DivisionId != this.divisionFilter)) return false;
            return true;
        });
        return filteredSlice;
    }

    private applyFiltersWithChallengeInfo(slice: ChallengeReportSliceWithChallengeInfo[]): ChallengeReportSliceWithChallengeInfo[] {
        let filteredSlice = slice.filter(row => {
            if (!row) return false;
            if ((this.countryFilter != -1) && (row.CountryId != this.countryFilter)) return false;
            if ((this.companyFilter != -1) && (row.CompanyId != this.companyFilter)) return false;
            if ((this.divisionFilter != -1) && (row.DivisionId != this.divisionFilter)) return false;
            return true;
        });
        return filteredSlice;
    }

    private prepareData() {
        let numUsersInReport: number = 0;
        // Sum up total users registered before window start date.
        // We are given these starting counts per division / company / country.
        // Irrelevant companies, countries, and divisions will have been filtered out in the previous step, so we just need to sum the remainder
        let runningCountUsers: number = this.startingUserCountsFiltered.reduce((value, prevSlice) => value + prevSlice.UserCount, 0);
        let runningCountUsersInChallenges: number = this.startingCountUsersInChallengesFiltered.reduce((value, prevSlice) => value + prevSlice.UserCount, 0);

        this.ChartData = [
            { label: $localize`:@@graphseries_totalusers:Total Users`, data: [] },
            { label: $localize`:@@graphseries_activeusers:Active Users`, data: [] },
            { label: $localize`:@@graphseries_usersinchallenge:Users in Challenge`, data: [] },
            { label: $localize`:@@graphseries_activeusersinchallenge:Active Users in Challenge`, data: [] },
        ];

        this.ChartLabels = [];

        this.challengeReportData.Buckets.forEach((chartBucket, i) => {
            this.ChartLabels[i] = this.getChartDisplayValueForBucket(chartBucket);

            // get running count for total users, and keep track of total users in this report
            let newUsers: number = this.sumValuesInBucket(this.totalUsersFiltered, chartBucket);
            runningCountUsers += newUsers;
            this.ChartData[0].data[i] = runningCountUsers;

            if (moment(chartBucket).isSameOrBefore(moment.now())) {
                // calculate running total users in challenges
                runningCountUsersInChallenges += this.sumChallengeUserValuesInBucket(this.usersInChallengesFiltered, chartBucket);
                // calculate number of users in challenges that have ended and remove them from the count
                let numUsersInEndedChallenges = this.sumChallengeUserValuesInBucketChallengeEnded(this.usersInChallengesFiltered, chartBucket);
                this.ChartData[2].data[i] = runningCountUsersInChallenges - numUsersInEndedChallenges;
            } else {
                // set to zero if date is after today (don't extrapolate data for the future)
                this.ChartData[2].data[i] = 0;
            }

            // add up the other counts
            const numActiveUsers: number = this.sumValuesInBucket(this.activeUsersFiltered, chartBucket);
            const numActiveUsersInChallenges: number = this.sumValuesInBucket(this.activeUsersInChallengesFiltered, chartBucket);

            // set chart values
            this.ChartData[1].data[i] = numActiveUsers;
            this.ChartData[3].data[i] = numActiveUsersInChallenges;

            numUsersInReport += Math.min(runningCountUsers, numActiveUsers, runningCountUsersInChallenges, numActiveUsersInChallenges);
        });

        this.hasData = (numUsersInReport >= 5);
        this.zeroUsers = (numUsersInReport === 0);
    }

    private getChartDisplayValueForBucket(bucket: Date): string {
        if (this.bucketType === BucketType.Day) {
            return moment(bucket).format('YYYY-MM-DD');
        } else if (this.bucketType === BucketType.Week) {
            const m = moment(bucket);
            const mnext = moment(bucket).add(6, 'days');
            let s = `${m.format('YYYY-MM-DD')} - ${mnext.format('YYYY-MM-DD')}`;
            return s;
        } else if (this.bucketType === BucketType.Month) {
            return moment(bucket).format('MMM YYYY');
        }
    }

    // sum all the values in a ChallengeReportSlice that are in the given time bucket
    private sumValuesInBucket(slices: ChallengeReportSlice[], bucket: Date): number {
         return slices.filter(slice => slice.Bucket.getTime() === bucket.getTime()).reduce((value, prevSlice) => value + prevSlice.UserCount, 0);
    }

    private sumChallengeUserValuesInBucket(slices: ChallengeReportSliceWithChallengeInfo[], bucket: Date): number {
        return slices.filter(slice => slice.Bucket.getTime() === bucket.getTime()).reduce((value, prevSlice) => value + prevSlice.UserCount, 0);
    }

    private sumChallengeUserValuesInBucketChallengeEnded(slices: ChallengeReportSliceWithChallengeInfo[], bucket: Date): number {
        return slices.filter(slice => slice.ChallengeEndDate.getTime() < bucket.getTime()).reduce((value, prevSlice) => value + prevSlice.UserCount, 0);
    }

}
