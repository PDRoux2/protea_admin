import { Component, OnInit, OnDestroy, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { HttpClient } from '@angular/common/http';
import { AppService } from '../../app.service';
import { Subject, Observable } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { SessionType } from '../../structures';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  templateUrl: 'supersessiontypes.component.html'
})
export class SuperSessionTypesComponent implements OnDestroy, OnInit {

  sessionTypeData: SessionType[];
  companyNames = {};

  @ViewChild(DataTableDirective) dtElement: DataTableDirective;
  @ViewChild('addSessionTypeModal') public addSessionTypeModal: ModalDirective;
  @ViewChild('editSessionTypeModal') public editSessionTypeModal: ModalDirective;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;

  selectedSessionType: SessionType;
  isLoading = true;

  dtOptions: DataTables.Settings = {
    "columnDefs": [{
      "targets": 0,
      "orderable": false
    }]
  };

  dtTrigger: Subject<any> = new Subject();


  constructor(private http: HttpClient, private service: AppService) {

  }

  editSessionType(sessiontype: SessionType) {
    this.selectedSessionType = sessiontype;
    (<any>document.getElementById("edit_name")).value = sessiontype.SessionTypeName;
    (<any>document.getElementById("edit_time")).value = sessiontype.TimeSeconds;
    (<any>document.getElementById("edit_theme")).value = sessiontype.Theme;
    this.editSessionTypeModal.show();
  }

  revealCompany(s: string) {
    document.getElementById("details").hidden = false;
    document.getElementById("details_name").innerText = s;
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  ngOnInit(): void {
    this.loadData();

  }

  isFirstLoad=true;

  loadData() {
    this.isLoading = true;
    this.service.getSessionTypes().subscribe(
      sessionTypeData => {
        this.sessionTypeData = sessionTypeData as SessionType[];
        if (this.isFirstLoad) this.dtTrigger.next();
        this.isFirstLoad=false;
        this.isLoading = false;
      },
      err => {
        console.log(err);
        this.isLoading = false;
      }
    );
  }

  themeName(t) {
    switch (+t) {
      case 0: return "Recover Theme";
      case 1: return "Perform Theme";
      case 2: return "Prepare Theme";
    }
  }

  createSessionType() {
    this.addSessionTypeModal.hide();

    let sessiontype: SessionType = {
      SessionTypeId: 0,
      SessionTypeName: (<any>document.getElementById("add_name")).value,
      TimeSeconds: (<any>document.getElementById("add_time")).value,
      Theme: (<any>document.getElementById("add_theme")).value
    }

    this.service.createSessionType(<JSON><any>sessiontype).subscribe(st => {
      this.loadData();
    });
  }


  updateSessionType() {
    let time = +(<any>document.getElementById("edit_time")).value;
    if ((time<=0)||(time>1000)||(isNaN(time))) {
      alert("Time in Seconds must be a number between 1 and 1000");
      return;
    }

    this.editSessionTypeModal.hide();

    let sessiontype: SessionType = this.selectedSessionType;

    sessiontype.SessionTypeName = (<any>document.getElementById("edit_name")).value;
    sessiontype.TimeSeconds = (<any>document.getElementById("edit_time")).value;
    sessiontype.Theme = (<any>document.getElementById("edit_theme")).value;

    this.service.updateSessionType(<JSON><any>sessiontype).subscribe(st => {
      this.loadData();
    });
  }

  deleteSessionType() {
    this.dangerModal.hide();
    this.editSessionTypeModal.hide();
    this.service.deleteSessionType(this.selectedSessionType.SessionTypeId).subscribe(user => {
       this.loadData();
    });
  }

}
