import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuperSessionTypesComponent } from './supersessiontypes.component';

const routes: Routes = [
  {
    path: '',
    component: SuperSessionTypesComponent,
    data: {
      title: 'Dashboard'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuperSessionTypesRoutingModule {}
