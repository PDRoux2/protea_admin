import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';

import { SuperSessionTypesComponent } from './supersessiontypes.component';
import { DataTablesModule } from 'angular-datatables';
import { SuperSessionTypesRoutingModule } from './supersessiontypes-routing.module';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    FormsModule,
    SuperSessionTypesRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    CommonModule,
    DataTablesModule,
    NgbModule,
    ModalModule.forRoot()
    
  ],
  declarations: [SuperSessionTypesComponent ]
})
export class SuperSessionTypesModule { }
