import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuperDemographicsComponent } from './superdemographics.component';

const routes: Routes = [
  {
    path: '',
    component: SuperDemographicsComponent,
    data: {
      title: 'Demographics'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuperDemographicsRoutingModule {}
