
import { Component } from '@angular/core';
import { AppService } from '../../app.service';

@Component({
  templateUrl: 'superdemogrphics.component.html'
})
export class SuperDemographicsComponent {

  public startDate = "";
  public endDate = "";
  public displayType = "0";
  public loading = true;
  public hasData = true;

  public dataSub = null;
  public lastData: any;


  public countryList = null;
  public languageList = null;

  public divisionList = null;
  public companyList = null;
  public sessionTypeList = null;
  public companyListFilt = null;
  public divisionListFilt = null;


  public companyFilter = -1;
  public divisionFilter = -1;
  public countryFilter = -1;
  public sessionTypeFilter = -1;
  public languageFilter = -1;

  public comChartData = null;
  public comChartLabels = null;

  public counChartData = null;
  public counChartLabels = null;

  public divChartData = null;
  public divChartLabels = null;

  public lanChartData = null;
  public lanChartLabels = null;


  /* tslint:enable:max-line-length */
  public mainChartOptions: any = {
    tooltips: {
      enabled: false,
    },
    responsive: true,
    maintainAspectRatio: false,
    elements: {
      line: {
        borderWidth: 2,
        tension: 0,

      },
      point: {
        radius: 5,
        hitRadius: 10,
        hoverRadius: 4,
        hoverBorderWidth: 3,
      }
    },
    legend: {
      display: true
    }
  };

  public mainChartColours: Array<any> = [
    {
      backgroundColor: ['rgba(38,77,136,.3)', 'rgba(1,146,96,.3)', 'rgba(248,180,37,.3)', 'rgba(119,110,54,.3)', 'rgba(228,56,132,.3)', 'rgba(94,57,141,.3)'],
      borderColor: ['rgba(38,77,136,1)', 'rgba(1,146,96,1)', 'rgba(248,180,37,1)', 'rgba(119,110,54,1)', 'rgba(228,56,132,1)', 'rgba(94,57,141,1)'],
    },
  ];
  public mainChartLegend = true;
  public mainChartType = 'pie';

  constructor(private service: AppService) {

  }

  ngOnInit(): void {
    this.service.getCountries().subscribe((c) => this.countryList = c);
    this.service.getCompanies().subscribe((c) => this.companyListFilt = this.companyList = c);
    this.service.getLanguages().subscribe((c) => this.languageList = c);
    this.service.getDivisions().subscribe((d) => this.divisionListFilt = this.divisionList = d);
    this.service.getSessionTypes().subscribe((t) => this.sessionTypeList = t);

    this.startDate = this.mkDate(Date.now() - 120 * 24 * 60 * 60 * 1000);
    this.endDate = this.mkDate(Date.now());
    setTimeout(() => this.loadData(), 3000);
  }

  loadData() {
    if (this.dataSub) this.dataSub.unsubscribe();
    this.loading = true;
    this.dataSub = this.service.getSessionData(2, this.startDate, this.endDate).subscribe((data: any) => {
      this.lastData = data.sort((a, b) => ((+a.userId) - (+b.userId)));
      this.filterData();
      this.loading = false;
    });
  }

  checkFilters() {
    this.companyListFilt = this.companyList.filter(row => ((this.countryFilter == -1) || (row.CountryId == this.countryFilter)));
    if (this.companyFilter!=-1)
       this.divisionListFilt = this.divisionList.filter(row => (row.CompanyId == this.companyFilter));
    else if  (this.countryFilter!=-1)
       this.divisionListFilt = this.divisionList.filter(row => {
         let com=this.companyList.find(e=>(e.CompanyId==row.CompanyId))
         return com.CountryId == this.countryFilter;
       });
    else
       this.divisionListFilt = this.divisionList;

    this.filterData();
  }


  filterData() {
    let filt = this.lastData.filter(row => {
      if ((this.countryFilter != -1) && (row.CountryId != this.countryFilter)) return false;
      if ((this.companyFilter != -1) && (row.CompanyId != this.companyFilter)) return false;
      if ((this.languageFilter != -1) && (row.LanguageId != this.languageFilter)) return false;
      if ((this.divisionFilter != -1) && (row.DivisionId != this.divisionFilter)) return false;
      if ((this.sessionTypeFilter != -1) && (row.SessionTypeId != this.sessionTypeFilter)) return false;
      return true;
    });

    //no add up the sections
    this.divChartData = [{ label: "Divisons", data: [], UIDs: [] }];
    this.divChartLabels = [];

    this.lanChartData = [{ label: "Languages", data: [], UIDs: [] }];
    this.lanChartLabels = [];

    this.comChartData = [{ label: "Company", data: [], UIDs: [] }];
    this.comChartLabels = [];

    this.counChartData = [{ label: "Country", data: [], UIDs: [] }];
    this.counChartLabels = [];


    let isVal = !(+this.displayType);
    let totalUsers=0;
    let lastUser=0;

    filt.forEach(row => {
      let val = (isVal) ? row.userSessions : -1;
      this.addBit(this.divChartData, this.divChartLabels, val,row.userId, this.divisionList.find(st => st.DivisionId == row.DivisionId).DivisionName);
      this.addBit(this.comChartData, this.comChartLabels, val,row.userId, this.companyList.find(st => st.CompanyId == row.CompanyId).CompanyName);
      this.addBit(this.counChartData, this.counChartLabels, val,row.userId, this.countryList.find(st => st.CountryId == row.CountryId).CountryName);
      let lang = this.languageList.find(st => st.LanguageId == row.LanguageId);
      if (lang) this.addBit(this.lanChartData, this.lanChartLabels, val,row.userId,lang.LanguageName);
      if (row.userId!=lastUser) totalUsers+=1;
      lastUser=row.userId;
    });
    this.hasData=true;//(totalUsers>5);
    this.updateLabels(this.divChartData,this.divChartLabels);
    this.updateLabels(this.comChartData,this.comChartLabels);
    this.updateLabels(this.counChartData,this.counChartLabels);
    this.updateLabels(this.lanChartData,this.lanChartLabels);
  }

  updateLabels(data,labels)
  {
    //sum the data
    let sum=data[0].data.reduce((tot,el)=>tot+el,0);
    //now calc every label
    data[0].data.forEach((v,i) => {
      labels[i]+=" "+Math.round(v*100/sum)+"%";
    });
  }

  addBit(data, labels, val, userId, label) {
    let b = labels.findIndex(l => l == label);
    if (b < 0) {
      b = labels.length;
      labels.push(label);
      data[0].data.push(0);
      data[0].UIDs.push(0);
    }
    if(val>=0) //this is a session count just do it
      data[0].data[b] += val;
    else //actually a user count if it's new
      data[0].data[b] += (data[0].UIDs[b]!=userId)?1:0;
    data[0].UIDs[b]=userId;
  }

  getSeriesName(row) {
    if (this.displayType == "0") return "Sessions";
    if (this.displayType == "10") return "Users";

    let val = (+this.displayType) % 10;

    if (val == 1) {
      let type = this.sessionTypeList.find(st => st.SessionTypeId == row.SessionTypeId);
      return type ? type.SessionTypeName : "UNKNOWN";
    }
    if (val == 2) {
      let div = this.divisionList.find(st => st.DivisionId == row.DivisionId);
      return div ? div.DivisionName : 'UNKNOWN';
    }
  }

  mkDate(dateMilli) {
    return new Date(dateMilli).toISOString().substring(0, 10)
  }

}
