import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ChartsModule } from 'ng2-charts';

import { SuperDemographicsComponent } from './superdemographics.component';
import { SuperDemographicsRoutingModule } from './superdemographics-routing.module';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    FormsModule,
    SuperDemographicsRoutingModule,
    ChartsModule,
    NgbModule,
    CommonModule,
  ],
  declarations: [ SuperDemographicsComponent ]
})
export class SuperDemographicsModule { }
