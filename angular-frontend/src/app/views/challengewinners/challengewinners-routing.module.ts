import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChallengeWinnersComponent } from './challengewinners.component';

const routes: Routes = [
  {
    path: '',
    component: ChallengeWinnersComponent,
    data: {
      title: 'Challenge Winners'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChallengeWinnersRoutingModule {}
