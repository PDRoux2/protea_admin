import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { ModalModule } from 'ngx-bootstrap/modal';


import { ChallengeWinnersComponent } from './challengewinners.component';
import { DataTablesModule } from 'angular-datatables';
import { ChallengeWinnersRoutingModule } from './challengewinners-routing.module';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    FormsModule,
    ChallengeWinnersRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    CommonModule,
    DataTablesModule,
    ModalModule.forRoot(),
    NgbModule,
  ],
  declarations: [ ChallengeWinnersComponent ],
  providers: [
  ],
  bootstrap: [ChallengeWinnersComponent],

})
export class ChallengeWinnersModule { }
