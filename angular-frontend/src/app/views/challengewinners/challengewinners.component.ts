import { Component, OnInit, OnDestroy, ViewChildren, QueryList, ViewChild, Renderer2, ComponentRef, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { AppService } from '../../app.service';

@Component({
  templateUrl: 'challengewinners.component.html'
})

export class ChallengeWinnersComponent implements OnDestroy, OnInit {

  constructor(private http: HttpClient, private service: AppService) {

  }

  ngOnDestroy(): void {
  }

  challenges = [];
  challengeTypes = [];
  winners = null;
  selCID = -1;
  selCh: any = null;
  loading = false;
  isTeam = false;

  ngOnInit(): void {
    this.service.getChallenges().subscribe(data => {
      //load challenges sorted by enddate (reversed) and then description
      this.challenges = data.sort((a, b) => (a.EndDate == b.EndDate)?((a.ChallengeName.toUpperCase()>b.ChallengeName.toUpperCase())?1:-1):((a.EndDate < b.EndDate) ? 1 : -1));
    });
  }

  loadData() {
    this.loading = true;
    let ch = this.challenges.find(ch => (ch.ChallengeId == this.selCID));
    this.isTeam = ((ch.ChallengeTypeId == 2) || (ch.ChallengeTypeId == 4));




    this.service.getChallengeWinners(this.selCID, this.isTeam).subscribe(result => {
      this.winners = result.sort((a, b) => ((a ? a.Points : 0) == (b ? b.Points : 0))?
                                         ((a.Description<b.Description)?-1:1)
                                        :(((a ? a.Points : 0) < (b ? b.Points : 0)) ? 1 : -1));
      this.selCh = ch;
      this.loading = false;
    });
  }


  isTeamC(ctid) {
    return ((ctid == 2) || (ctid == 4))
  }

  isMax(ctid) {
    return ((ctid == 3) || (ctid == 4))
  }

}
