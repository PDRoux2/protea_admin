import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { DataTablesModule } from 'angular-datatables';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';

import { SuperUsersComponent } from './superusers.component';
import { SuperUsersRoutingModule } from './superusers-routing.module';

@NgModule({
  imports: [
    SuperUsersRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    CommonModule,
    DataTablesModule,
    ModalModule.forRoot(),
    NgbModule
  ],
  declarations: [ SuperUsersComponent ]
})
export class SuperUsersModule { }
