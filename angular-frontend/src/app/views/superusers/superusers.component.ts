import { Component, OnInit, OnDestroy, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppService } from '../../app.service';
import { AuthenticationService } from '../../authentication.service';
import { Subject, Observable } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { UserInfo, Company, Division } from '../../structures';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    templateUrl: 'superusers.component.html'
})
export class SuperUsersComponent {
    usersData: UserInfo[];

    filteredDivisionList: Division[] = [];
    divisionData: Division[];
    companyData: Company[];

    selectedUser: UserInfo;
    selectedCompany = 0;
    editCompany = 0;

    divisionNames = {};
    divisionCompany = {};
    divisionCompanyId = {};
    companyNames = {};

    edittedUser = 0;
    edittingRow: any;
    editRow: any;
    editNode: any;
    accessLevel = 0;

    isLoading = true;

    @ViewChild(DataTableDirective) dtElement: DataTableDirective;
    @ViewChild('editUserModal') public editUserModal: ModalDirective;
    @ViewChild('dangerModal') public dangerModal: ModalDirective;

    dtOptions: DataTables.Settings = {
        autoWidth: false,
        columnDefs: [{
            targets: 0,
            orderable: false,
            width: "1em",
        }, {
            targets: [1, 2, 3, 4, 6, 7, 8, 9],
            width: "2em",
        }, {
            targets: 5,
            width: "3em",
        },]
    };

    dtTrigger: Subject<any> = new Subject();

    constructor(private http: HttpClient, private service: AppService, private authService: AuthenticationService) {
        this.accessLevel = authService.getUserDetail().AccessLevel;
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }

    ngOnInit(): void {
        this.isLoading = true;
        this.loadData();
        this.editRow = document.getElementById("edit_user");
    }

    mapAccessLevel(lev) {
        switch (+lev) {
            case 0: return $localize`:@@access level 0:Normal`;
            case 1: return $localize`:@@access level 1:Company Manager`;
            case 2: return $localize`:@@access level 2:Country Manager`;
            case 3: return $localize`:@@access level 3:Super User`;
        }
    }

    loadData() {
        this.service.getCompanies().subscribe(
            companyData => {
                this.companyData = companyData;
                (<Company[]><any>companyData).forEach(element => {
                    this.companyNames[element["CompanyId"]] = element["CompanyName"];
                });
                this.service.getDivisions().subscribe(
                    divisionData => {
                        this.divisionData = divisionData;
                        this.divisionData.forEach(element => {
                            this.divisionNames[element["DivisionId"]] = element["DivisionName"];
                            this.divisionCompany[element["DivisionId"]] = this.companyNames[element["CompanyId"]];
                            this.divisionCompanyId[element["DivisionId"]] = element["CompanyId"];
                        });

                        this.service.getUsers().subscribe(
                            usersData => {
                                this.usersData = usersData as UserInfo[];
                                usersData.forEach(element => {
                                    element["DivisionName"] = this.divisionNames[element["DivisionId"]];
                                    element["CompanyName"] = this.divisionCompany[element["DivisionId"]];
                                });
                                this.dtTrigger.next();
                                this.isLoading = false;
                            }, err => {
                                console.log(err);
                            }
                        );
                    }, err => {
                        console.log(err);
                    }
                );
            }, err => {
                console.log(err);
            }
        );
    }

    companyChange(c) {
        this.selectedCompany = c;
        this.filteredDivisionList = [];

        this.divisionData.forEach(element => {
            if (element.CompanyId == c) {
                this.filteredDivisionList.push(element);
            }
        });

        this.filteredDivisionList.sort();
    }

    editUser(user: UserInfo) {
        this.selectedUser = user;

        (<any>document.getElementById("edit_username")).value = user.UserName;
        (<any>document.getElementById("edit_firstname")).value = user.FirstName;
        (<any>document.getElementById("edit_lastname")).value = user.LastName;
        (<any>document.getElementById("edit_email")).value = user.EmailAddress;
        (<any>document.getElementById("edit_company")).value = this.divisionCompanyId[user.DivisionId];
        this.selectedCompany = this.divisionCompanyId[user.DivisionId];
        (<any>document.getElementById("edit_division")).value = user.DivisionId;
        (<any>document.getElementById("edit_access")).selectedIndex = user.AccessLevel;
        this.companyChange(this.selectedCompany);
        this.editUserModal.show();
    }

    deleteUser() {
        this.dangerModal.hide();
        this.editUserModal.hide();
        this.service.deleteUser(this.selectedUser.UserId).subscribe(user => {
            let index = this.usersData.indexOf(this.selectedUser);
            if (index !== -1) {
                this.usersData.splice(index, 1);
            }
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.dtTrigger.next();
            });
        });
    }

    updateUser() {
        this.editUserModal.hide();

        let user: UserInfo = this.selectedUser;

        user.UserName = (<any>document.getElementById("edit_username")).value;
        user.FirstName = (<any>document.getElementById("edit_firstname")).value;
        user.LastName = (<any>document.getElementById("edit_lastname")).value;
        user.EmailAddress = (<any>document.getElementById("edit_email")).value;
        user.DivisionId = (<any>document.getElementById("edit_division")).value;
        user.AccessLevel = (<any>document.getElementById("edit_access")).value;

        user.DivisionName = this.divisionNames[user.DivisionId];
        user.CompanyName = this.divisionCompany[user.DivisionId];

        this.service.updateUser(<JSON><any>user, true).subscribe(user => {
            console.log("user saved", user);
        });
    }
}
