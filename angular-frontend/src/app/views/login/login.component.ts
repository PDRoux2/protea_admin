import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../../authentication.service'

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent {
  public user = '';
  public password = '';
  public errorResponse='';
  public workingMessage='';

  constructor(private router: Router,
              private authService: AuthenticationService) {

              }

  async login() {
    this.errorResponse='';
    this.workingMessage="Acquiring Authorisation";
    let status=await this.authService.login(this.user,this.password);
    this.workingMessage="";
    console.log("status here:"+status)
    if (status=="OK") {
      console.log("execute navigation");
      this.workingMessage="Starting Application";
      setTimeout(()=>this.router.navigate(['/']),1000);
    } else
      this.errorResponse=status;
  }

  resetPassword() {
    this.authService.resetPassword(this.user);
    alert("An email has been sent to "+this.user+" to reset your password")
  }

}
