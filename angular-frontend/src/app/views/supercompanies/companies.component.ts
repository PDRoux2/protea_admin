import { Component, OnInit, OnDestroy, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { HttpClient } from '@angular/common/http';
import { AppService } from '../../app.service';
import { Subject , Observable } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { Company } from '../../structures';
import {ModalDirective} from 'ngx-bootstrap/modal';


@Component({
  templateUrl: 'companies.component.html'
})
export class CompaniesComponent implements OnDestroy, OnInit {

    companyData: Company[];

    @ViewChild(DataTableDirective) dtElement: DataTableDirective;
    @ViewChild('addCompanyModal') public addCompanyModal: ModalDirective;
    @ViewChild('editCompanyModal') public editCompanyModal: ModalDirective;
    @ViewChild('dangerModal') public dangerModal: ModalDirective;

    selectedCompany : Company;
    comUsers=[];
    divisions=[];
    isLoading=true;

    dtOptions: DataTables.Settings = {
      "columnDefs": [ {
      "targets": 0,
      "orderable": false
      } ]};

    dtTrigger: Subject<any> = new Subject();
    countries=[];

    constructor(private http: HttpClient, private service: AppService) {

    }



    editCompany(company : Company){
      this.selectedCompany = company;

      (<any>document.getElementById("edit_name")).value = company.CompanyName;
      (<any>document.getElementById("edit_country")).value = company.CountryId;

      //check out the users we need
      this.comUsers=[];
      this.service.getUsers().subscribe(data=>{
        this.comUsers=data.filter((u)=> {
          if (u.AccessLevel!==1) return false; //not interested in ground elevel users
          //get company from division
          let div=this.divisions.find(d=>d.DivisionId==u.DivisionId);
          if (!div) return false; //no such division
          return div.CompanyId==company.CompanyId;
        });
      })

      this.editCompanyModal.show();
    }

    countryName(id:number) {
      let c=this.countries.find(c=>c.CountryId==id)
      return c?c.CountryName:"Unknown";
    }


    postCompany(){
      let company = {};
      company["CompanyName"] = (<any>document.getElementById("add_name")).value;
      company["CountryId"] = +(<any>document.getElementById("add_country")).value;
      if (!this.checkCompanyNameUnique(company)) return;
      this.addCompanyModal.hide();

      this.service.createCompany(<JSON><any>company).subscribe(div => {
        this.companyData.push(<Company><any>div);
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.dtTrigger.next();
        });
      });
    }

    deleteCompany(){
      this.dangerModal.hide();
      this.editCompanyModal.hide();
      this.service.deleteCompany(this.selectedCompany.CompanyId).subscribe(user => {
      let index = this.companyData.indexOf(this.selectedCompany);
        if (index !== -1) {
          this.companyData.splice(index, 1);
        }
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.dtTrigger.next();
        });
      });
    }

    updateCompany(){
      let company : Company = this.selectedCompany;

      company.CompanyName = (<any>document.getElementById("edit_name")).value;
      company.CountryId = +(<any>document.getElementById("edit_country")).value;

      if (!this.checkCompanyNameUnique(company)) return;
      this.editCompanyModal.hide();

      this.service.updateCompany(<JSON><any>company).subscribe();
    }

    checkCompanyNameUnique(nd)
    {
      console.log(nd,this.companyData);
      let match=this.companyData.find(d=>((nd.CompanyName==d.CompanyName)&&(nd.CompanyId!=d.CompanyId)));
      if (match) alert("A company with this name already exists.")
      return !match;
    }


    ngOnDestroy(): void {
      // Do not forget to unsubscribe the event
      this.dtTrigger.unsubscribe();
    }

    ngOnInit(): void {
      this.isLoading=true;
      this.service.getDivisions().subscribe(data =>  this.divisions = data );
      this.service.getCountries().subscribe(data =>  {
        this.countries = data;
        console.log(this.countries);
        this.service.getCompanies().subscribe(
          companyData =>  {
            this.companyData = companyData as Company[];
            this.dtTrigger.next();
            this.isLoading=false;
         },
         err => {
          console.log(err);
         }
        );
      });
    }
}
