import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';

import { CompaniesComponent } from './companies.component';
import { DataTablesModule } from 'angular-datatables';
import { CompaniesRoutingModule } from './companies-routing.module';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    FormsModule,
    CompaniesRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    CommonModule,
    DataTablesModule,
    NgbModule,
    ModalModule.forRoot()
    
  ],
  declarations: [ CompaniesComponent ]
})
export class CompaniesModule { }
