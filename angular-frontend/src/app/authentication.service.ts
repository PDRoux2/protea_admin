﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { AngularFireAuth } from '@angular/fire/auth';
import 'firebase/auth';


import { tap, switchMap, take } from 'rxjs/operators';
import { defer, ReplaySubject } from 'rxjs';

import { User,Division,Company } from './structures'
import { AppService } from './app.service'



@Injectable()
export class AuthenticationService {

  constructor(private afAuth: AngularFireAuth,private appService:AppService,private router: Router) {
    this.afAuth.authState.subscribe(async user => {
      if (user) {
        console.log("Awaiting User")
        await this.loadUser(user)
      } else {
        console.log("no user")
        this.userName=null;
        this.user=null;
        this.isLoggedIn=false;
      }
      this.userChannel.next(this.user);
    });
  }

  private isLoggedIn = false;
  private userName: string;
  private user: any;
  private userDetail: User;

  public userChannel=new ReplaySubject(1);

  public async loadUser(user) {
      this.userName=user.email;
      this.user=user;
      this.isLoggedIn=true;
      console.log("user "+this.user.uid);
      return this.appService.getUserByUid(this.user.uid).
                pipe(tap((user)=>{
                  console.log("user detail ",user);
                  if ((!user)||(user.AccessLevel<1)) {
                    this.logout();
                    throw "User not Authorised for this console";
                  }
                  this.userDetail=user;
                  if (this.userDetail.AccessLevel<2)
                    this.appService.forceCompany({
                          CompanyId : user.CompanyId,
                          CompanyName : user.CompanyName,
                          CountryId : user.CountryId,
                        });
                })).toPromise();
  }


  public hasUser() {
    return this.isLoggedIn;
  }

  public hasUserAuth() {
    return this.userChannel.pipe(take(1));
  }

  public getUserDetail() {
    return this.userDetail;
  }


  public getToken()
  {
    return defer(()=>this.user.getIdToken());
  }

  public async login(email: string, password: string) {
    return this.afAuth.signInWithEmailAndPassword(email, password)
      .then(async (result) => {
        console.log("before");
        try {
          await this.loadUser(result.user);
        } catch (e) {
          return "User not authorised to use admin console";
        }
        return "OK";
      }).catch((error) => {
        return error.message;
      })
  }

  public resetPassword(email:string)
  {
    return this.afAuth.sendPasswordResetEmail(email);
  }

  public logout() {
    this.user.loggedIn=false;
    console.log("signing out");
    this.afAuth.signOut();
    setTimeout(()=>{
      console.log("signed out");
      this.router.navigate(['/login']);
    },1000);
  }

  public currentUser() { return this.userName; }

}
