import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { AuthenticationService } from './authentication.service'
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RouteGuardService implements CanActivate {

  constructor(private router: Router,
    private authService: AuthenticationService) { }

  canActivate(next: ActivatedRouteSnapshot) {
    return this.authService.hasUserAuth().pipe(
      map((auth)=>{
        if (!auth) {
          console.log("forcing login - no auth")
          this.router.navigate(['/login']);
          return false;
        }
        return true;
      }));


  }

}
