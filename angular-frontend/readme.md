# Protea Japan Admin Console

## Introduction

The console is a angular application built as a website for use by Protea staff and senior client company executives to manage and monitor the usage of the android application.

## Technologies

The application is build on angular 9+ and uses:
UI: coreui (bootstrap based)
Auth: google Firebase Auth
Charts: charts.js
i18n: the new angular i18n xlf based solution

All data is received from the protea backend using standard https calls to the backend api.

## Local Set Up And Run

all scripts are based from the angular-frontend directory.
Use `ng serve` to build and locally serve (on localhost:4200) the staging version
Use `ng serve --configuration=production` to build and locally serve the production version (BE CAREFUL, this touches the production database)

The environments directory defines which servers are used for which builds both as auth and API endpoints.

## i18n

If making a change to elements that are tagged with i18n, run:

`npm run i18n:extract`

This will generate a messages.xlf file in src\locale
View diff:
  * if an ID has been updated, find the old ID in messages.ja.xlf and update the ID there as well
  * if new translations have been added to messages.xlf that are not in messages.ja.xlf, get the translations and add them

To confirm changes to the japanese, run `ng serve --configuration=ja` which will serve a japanese version on localhost
To view the japanese version of the staging site, visit https://protea-work-active-staging.firebaseapp.com/ja (similar for live version)

English and Japanese version must be built separately and copied together to one hosting

## Deployment
The systems are currently deployed to a firebase hosting service.

npm scripts have been written which:
- Build the english version
- Build the japanese version
- copy the ja deployment to inside the english Deployment
- select the correct firebase instance
- deploy the code

use `npm run deploy-stg` to build and deploy the staging version to the staging server.
use 'npm run deploy-release' to build and deploy the production version to the live server.
